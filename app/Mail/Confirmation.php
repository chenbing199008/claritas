<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Confirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $form;

    public function __construct($form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        "office" => 1
//  "name" => "chenbing"
//  "companyName" => "star-net"
//  "tel" => "11"
//  "email" => "chenbing199008@gmail.com"
//  "message" => "test"
//        dd($this->form);
        $view = 'ConfirmationForEn';
        $subject = 'Thank you for contacting us';
        $from = 'info@claritas-marketing.com';

        if (app()->getLocale() !== "en") {
            $view = 'ConfirmationForJp';
            $subject = 'お問い合わせありがとうございます';
        }

        // Tokyo Office
        if ($this->form['office'] == 1) {
            $from = 'japan@claritas-marketing.com';
        }

        return $this->from($from)->subject($subject)->view('emails.'.$view)->with('info', $this->form)->with('baseUrl', $_ENV['APP_URL']);
    }
}
