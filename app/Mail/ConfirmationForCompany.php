<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmationForCompany extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $form;

    public function __construct($form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = 'ConfirmationForEnInCompany';
        $subject = 'You have received the form';
        $from = 'info@claritas-marketing.com';

        if (app()->getLocale() !== "en") {
            $view = 'ConfirmationForJpInCompany';
            $subject = 'お問合せを受け付けました';
        }

        // Tokyo Office
        if ($this->form['office'] == 1) {
            $from = 'japan@claritas-marketing.com';
        }

        return $this->from($from)->subject($subject)->view('emails.'.$view)->with('info', $this->form)->with('baseUrl', $_ENV['APP_URL']);
    }
}
