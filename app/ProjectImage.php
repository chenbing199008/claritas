<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectImage extends Model
{
    use LogsActivity;

    const STATUSES = [
        'published',
        'unpublished'
    ];
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name_en', 'name_jp', 'image', 'project_id', 'status', 'sort_order'];

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
