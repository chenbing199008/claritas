<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Blog extends Model
{
    use LogsActivity;

    const STATUSES = [
        'published',
        'unpublished'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_en',
        'title_jp',
        'excerpt_en',
        'excerpt_jp',
        'post_en',
        'post_jp',
        'image_thumb',
        'image_large',
        'posted_date',
        'status'
    ];

    protected $casts = [
        'posted_date' => 'datetime',
    ];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
