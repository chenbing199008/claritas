<?php

namespace App\Http\Controllers;

use App\Area;
use App\Blog;
use App\Country;
use App\Gallery;
use App\Mail\Confirmation;
use App\Mail\ConfirmationForCompany;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class FrontendController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $videoUrl = setting('HOME_PAGE_YOUTUBE_VIDEO_URL');

        return view('frontend.index', ['menu' => 'index', 'videoUrl' => $videoUrl]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function work()
    {
        $field = "name_" . LANG_FIELD;
        $projects = [];
        $projectsFromDB = Project::with('categories', 'images')->where('status', 'published')
            ->with('country', 'images')
            ->orderBy('year', 'DESC')
            ->orderBy('name_en', 'ASC')
            ->get();
        foreach ($projectsFromDB as $project) {
            $categories = $project
                ->categories
                ->sortBy('sort_order', 1)
                ->pluck($field)
                ->toArray();
            $eventCategories = $project
                ->eventCategories
                ->sortBy('sort_order', 1)
                ->pluck($field)
                ->toArray();

            $projects[] = [
                'id' => $project->id,
                'name' => $project->$field,
                'location' => $project->address,
                'year' => $project->year,
                'services' => $categories,
                'eventCategories' => $eventCategories,
                'event_category_free_description' => $project->event_category_free_description,
                'country' => $project->country->name_en,
                'country_code' => Str::camel($project->country->name_en),
                'area_code' => Str::camel($project->country->area->getName())
            ];
        }
        return view('frontend.work', ['menu' => 'work', 'projects' => $projects]);
    }

    public function gallery()
    {
        $galleries = Gallery::orderBy('sort_order', 'ASC')->get();
        return view('frontend.gallery', ['menu' => 'gallery', 'galleries' => $galleries]);
    }

    public function contact()
    {
//        return view('frontend.contact', ['menu' => 'contact', 'isJp' => app()->getLocale() === "ja"]);
        return view('frontend.contact', ['menu' => 'contact', 'language' => app()->getLocale()]);
    }


    public function photoBlog()
    {
        if (app()->getLocale() === "en") {
            return redirect('/');
        }
        $blogs = Blog::where(['status' => 'published'])
            ->orderBy('posted_date', 'DESC')->get();
        return view('frontend.photo-blog', [
            'menu' => 'photo_blog',
            'blogs' => $blogs
        ]);
    }

    public function data()
    {
        $cacheKey = 'data.js' . LANG_FIELD;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $data = [
            'areaImageBaseUrl' => '/img/area/',
            'areaCoords' => [
                'russia' => '110, 54, 133, 64, 161, 52, 176, 54, 182, 44, 202, 44, 240, 27, 279, 42, 368, 50, 403, 64, 379, 77, 352, 85, 341, 108, 338, 94, 349, 80, 335, 88, 317, 85, 300, 99, 309, 101, 310, 112, 297, 128, 289, 128, 291, 120, 295, 120, 299, 114, 291, 114, 285, 111, 281, 101, 270, 101, 268, 111, 259, 108, 252, 113, 232, 104, 231, 109, 224, 107, 214, 112, 201, 106, 189, 97, 164, 100, 165, 108, 146, 105, 139, 113, 143, 119, 138, 122, 142, 129, 127, 129',
                'asia' => '138, 112, 147, 106, 165, 109, 165, 100, 181, 98, 194, 100, 200, 108, 212, 112, 221, 109, 230, 111, 231, 106, 240, 111, 249, 112, 262, 111, 268, 111, 271, 102, 283, 106, 286, 111, 290, 115, 292, 117, 298, 114, 296, 122, 287, 127, 294, 128, 286, 134, 288, 141, 311, 121, 318, 125, 313, 129, 309, 144, 302, 147, 291, 152, 283, 144, 272, 131, 266, 138, 274, 153, 264, 169, 243, 174, 252, 186, 273, 176, 284, 198, 305, 216, 310, 218, 309, 228, 302, 228, 276, 231, 244, 226, 227, 203, 230, 190, 220, 169, 200, 182, 195, 196, 182, 172, 171, 164, 166, 162, 164, 152, 165, 142, 155, 136, 152, 141, 151, 132, 144, 126, 149, 123, 150, 119, 142, 120',
                'europe' => "63, 80, 83, 56, 102, 51, 111, 55, 124, 114, 143, 130, 136, 139, 115, 140, 104, 136, 77, 121, 82, 134, 70, 122, 55, 137, 38, 138, 38, 126, 52, 127, 47, 112, 34, 105, 34, 99, 42, 95, 44, 108, 56, 105, 50, 94",
                'middleEast' => "103, 135, 110, 129, 142, 129, 144, 132, 141, 136, 148, 142, 158, 136, 164, 143, 164, 156, 167, 162, 154, 158, 138, 152, 148, 167, 154, 164, 161, 169, 158, 175, 134, 185, 132, 175, 113, 156, 118, 166, 102, 170, 102, 150, 111, 151, 119, 139",
                'africa' => "22, 171, 43, 142, 73, 140, 70, 148, 88, 154, 90, 148, 102, 149, 102, 171, 119, 168, 133, 193, 144, 190, 124, 213, 126, 240, 118, 248, 117, 256, 88, 280, 72, 247, 80, 233, 69, 203, 59, 198, 45, 203, 26, 194",
                "america" => "411, 58, 428, 50, 469, 59, 480, 54, 487, 45, 488, 38, 492, 32, 506, 24, 503, 30, 515, 32, 517, 29, 524, 36, 520, 24, 537, 30, 539, 26, 551, 34, 569, 34, 573, 28, 554, 30, 550, 24, 541, 16, 551, 7, 580, 3, 605, 6, 582, 20, 578, 25, 555, 40, 569, 38, 606, 64, 601, 68, 595, 65, 600, 75, 596, 80, 577, 70, 584, 64, 575, 52, 569, 52, 569, 62, 561, 68, 571, 73, 562, 76, 556, 70, 544, 84, 549, 93, 569, 99, 568, 104, 571, 107, 573, 96, 578, 94, 574, 86, 574, 77, 586, 79, 593, 90, 600, 83, 605, 94, 612, 97, 614, 106, 605, 111, 600, 110, 599, 119, 585, 126, 579, 138, 579, 141, 566, 153, 569, 164, 559, 152, 540, 156, 538, 172, 544, 177, 551, 177, 553, 171, 559, 172, 555, 182, 563, 181, 565, 191, 577, 196, 588, 189, 601, 193, 626, 204, 626, 213, 655, 225, 642, 254, 630, 258, 629, 265, 619, 277, 597, 291, 599, 308, 589, 320, 596, 324, 585, 322, 580, 314, 589, 246, 577, 238, 567, 221, 569, 214, 575, 203, 565, 195, 561, 185, 549, 180, 537, 181, 504, 146, 515, 167, 492, 139, 488, 116, 479, 98, 454, 83, 445, 83, 413, 103, 428, 86, 411, 77, 408, 69"]
        ];
        $field = "name_" . LANG_FIELD;
        $areasFromDB = Area::orderBy('sort_order', 'ASC')->get();

        $areas = [];
        foreach ($areasFromDB as $area) {
            $areas[] = [
                'label' => $area->name_en,
                'name' => Str::camel($area->getName()),
            ];
        };
        $data['areas'] = $areas;

        $countriesFromDB = Country::with('area')
            ->orderBy('name_en', 'ASC')
            ->withCount('projects')
            ->has('projects', '>', 0)
            ->get();
        $countries = [];
        foreach ($countriesFromDB as $country) {
            $countries[] = [
                'label' => $country->$field,
                'name' => Str::camel($country->name_en),
                'area' => Str::camel($country->area->getName()),
            ];
        }

        $data['countries'] = $countries;

        $projects = [];
        $projectsFromDB = Project::with('categories', 'images')->where('status', 'published')
            ->with('country', 'images')
            ->orderBy('year', 'DESC')
            ->orderBy('sort_order', 'ASC')
            ->orderBy('name_en', 'ASC')
            ->get();
        foreach ($projectsFromDB as $project) {
            $categories = $project
                ->categories
                ->sortBy('sort_order', 1)
                ->pluck($field)
                ->toArray();
            $images = $project
                ->images
                ->sortBy('sort_order', 1)
                ->where('status', 'published')
                ->toArray();
            $images = array_values($images);
            $projectImages = array_map(function ($image) use ($field) {
                $url = url("/storage/project-image/" . $image['image']);
                return [
                    'title' => $image[$field],
                    'sort_order' => $image['sort_order'] ?? '',
                    'large' => $url,
                    'thumbnail' => $url
                ];
            }, $images);
            $projects[] = [
                'id' => $project->id,
                'name' => $project->$field,
                'location' => $project->address,
                'year' => $project->year,
                'services' => $categories,
                'images' => $projectImages,
                'country' => Str::camel($project->country->name_en)
            ];
        }
        $data['projects'] = $projects;

        Cache::put($cacheKey, $data, 0);// Need to fix this cache to store the data properly

        header('Content-Type: application/javascript');
        return view('frontend.data', compact('data'));
    }

    public function contactSent()
    {
        $user = trim(\request('email'));
        if (!$user) {
            return false;
        }

        Mail::to($user)->send(new Confirmation(\request()->all()));

        $from = 'info@claritas-marketing.com';
        if (\request('office') == 1) {
            $from = 'japan@claritas-marketing.com';
        }
        Mail::to($from)->send(new ConfirmationForCompany(\request()->all()));

        return ['url' => url('contact_sent_success')];
    }

    public function contactSentSuccess()
    {
        return view('frontend.sent-email-success', ['menu' => 'contact']);
    }
}
