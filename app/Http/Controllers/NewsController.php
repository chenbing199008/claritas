<?php

namespace App\Http\Controllers;

use App\News;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

class NewsController extends Controller
{
    public function mobileTimeLines()
    {
        $posts = News::orderBy('published_at', 'desc')->get()->toArray();
        if (!$posts) {
            return response()->json(['timeLines' => []]);
        }
        $tranTimeLines = [];
        foreach ($posts as $post) {
            $year = date('Y', strtotime($post['published_at']));
            $month = date('m', strtotime($post['published_at']));

            if (isset($tranTimeLines[$year][$month])) {
                continue;
            }

            $tranTimeLines[$year][] = $month;
        }

        $timeLines = [];

        foreach ($tranTimeLines as $year => $months) {
            $months = array_values(array_unique($months));
            foreach ($months as $month) {
                $timeLines[] = ['link' => $year . '-' . $month, 'label' => $year . ' ' . (int)$month . '月'];
            }
        }

        return response()->json(['timeLines' => $timeLines]);


    }

    public function index()
    {
        $language = app()->getLocale();

        if ($language === 'en') {
            return redirect('/');
        }


        $agent = new Agent();

        if ($agent->isMobile()) {
            return view('frontend.news_mobile', [
                'menu' => 'industry_news',
            ]);
        }

        return view('frontend.news', [
            'menu' => 'industry_news',
        ]);
    }

    public function latest()
    {

        $latest = News::orderBy('published_at', 'desc')->first();
        if (!$latest) {
            return response()->json(['latest' => []]);
        }


        $latest->created = date('Y/m/d', strtotime($latest->published_at));
        $latest->cutSummary = mb_substr($latest->summary, 0, 100);
        $latest->tags = explode(',', $latest->hash_tags);
//        $latest->body = nl2br($latest->body);


        return response()->json(['latest' => $latest]);
    }


    public const EACH_PAGE_SHOW_NUMBER = 10;

    public function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[ $i ] = self::convert_from_latin1_to_utf8_recursively($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

            return $dat;
        } else {
            return $dat;
        }
    }

    public function posts()
    {
        $number = self::EACH_PAGE_SHOW_NUMBER;

        $agent = new Agent();
        if ($agent->isMobile()) {
            $number = 20;
        }

        $query = \request('query');
        $page = $query['page'];

        $offset = ($page - 1) * $number;

        $latestId = News::orderBy('published_at', 'desc')->value('id');

        $builder = News::offset($offset)->limit($number);
        $builderTotal = '';

        if ($query['date']) {
            $builder->where('published_at', 'like', "%" . $query['date'] . "%");
            $builderTotal = News::where('published_at', 'like', "%" . $query['date'] . "%");
        }

        if ($query['keyWord']) {
//            $keyword = mb_convert_encoding($query['keyWord'], 'UTF-8', 'UTF-8');
            $keyword = $query['keyWord'];

            $builder = $builder->where(function ($el) use ($keyword) {
                $el->where('title', 'like', "%{$keyword}%")->orWhere('body', 'like', "%{$keyword}%");
            });

            if (!$builderTotal) {
                $builderTotal = new News();
            }

            $builderTotal = $builderTotal->where(function ($el) use ($keyword) {
                $el->where('title', 'like', "%{$keyword}%")->orWhere('body', 'like', "%{$keyword}%");
            });
        }

        if (!$query['date'] && !$query['keyWord']) {
            $builder->where('id', '<>', $latestId);
        }

        if (!$builderTotal) {
            $total = News::where('id', '<>', $latestId)->count();
        } else {
            $total = $builderTotal->where('id', '<>', $latestId)->count();
        }


        $posts = $builder->orderBy('published_at', 'desc')->get()->toArray();
//        var_dump(111);
//        die();
        if (!$posts) {
            return response()->json(['posts' => []]);
        }

        foreach ($posts as &$post) {
//            $post = $this->convert_from_latin1_to_utf8_recursively($post);
            $post['created_at'] = date('Y/m/d', strtotime($post['published_at']));
            $post['cutSummary'] = mb_substr($post['summary'], 0, 100);
            $post['cutTitle'] = mb_substr($post['title'], 0, 100);
            $post['tags'] = explode(',', $post['hash_tags']);
//            $post['body'] = nl2br($post['body']);
            $post['showAll'] = false;
        }

//        echo json_encode(['posts' => $posts, 'total' => $total]);

//        var_dump(json_encode(['posts' => $posts, 'total' => $total]));
//        var_dump($posts);
//        die();

        return response()->json(['posts' => $posts, 'total' => $total]);
    }

    public function timeLines()
    {
        $posts = News::orderBy('published_at', 'desc')->get()->toArray();
        if (!$posts) {
            return response()->json(['timeLines' => []]);
        }
        $tranTimeLines = [];
        foreach ($posts as $post) {
            $year = date('Y', strtotime($post['published_at']));
            $month = date('m', strtotime($post['published_at']));

            if (isset($tranTimeLines[$year][$month])) {
                continue;
            }

            $tranTimeLines[$year][] = $month;
        }

        foreach ($tranTimeLines as $year => $months) {
            $months = array_values(array_unique($months));
            $tranTimeLines[$year] = [];
            foreach ($months as $month) {
                $tranTimeLines[$year]['months'][] = ['link' => $year . '-' . $month, 'month' => (int)$month . '月'];
            }
            $tranTimeLines[$year]['year'] = $year;
        }

        krsort($tranTimeLines, SORT_NUMERIC);

        return response()->json(['timeLines' => array_values($tranTimeLines)]);

    }


}
