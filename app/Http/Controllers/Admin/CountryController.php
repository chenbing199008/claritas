<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Country;
use App\Area;
use App\Project;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $country = Country::select('countries.*')
                ->join('areas', 'areas.id', '=', 'countries.area_id')
                ->orWhere('countries.name_en', 'LIKE', "%$keyword%")
                ->orWhere('countries.name_jp', 'LIKE', "%$keyword%")
                ->orWhere('areas.name_en', 'LIKE', "%$keyword%")
                ->orWhere('areas.name_jp', 'LIKE', "%$keyword%")
                ->orderBy('countries.name_en')->paginate($perPage);
        } else {
            $country = Country::orderBy('name_en', 'ASC')->with('area')->paginate($perPage);
        }

        return view('admin.country.index', compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $areas = Area::all();
        $areas = $areas->pluck('name_en', 'id')->toArray();

        return view('admin.country.create', compact('areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_en' => 'required',
			'name_jp' => 'required',
			'area_id' => 'required'
		]);
        $requestData = $request->all();
        
        Country::create($requestData);

        return redirect('admin/country')->with('flash_message', 'Country added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $country = Country::findOrFail($id);

        return view('admin.country.show', compact('country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        $areas = Area::all();
        $areas = $areas->pluck('name_en', 'id')->toArray();

        return view('admin.country.edit', compact('country', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_en' => 'required',
			'name_jp' => 'required',
			'area_id' => 'required'
		]);
        $requestData = $request->all();
        
        $country = Country::findOrFail($id);
        $country->update($requestData);

        return redirect('admin/country')->with('flash_message', 'Country updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $noOfProject = Project::where('country_id', $id)->count();
        if ($noOfProject) {
            return redirect('admin/country')->with('flash_error', 'Sorry. Cant delete country because it has projects');
        }

        Country::destroy($id);

        return redirect('admin/country')->with('flash_message', 'Country deleted!');
    }
}
