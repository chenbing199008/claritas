<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;
use App\ProjectImage;
use Illuminate\Http\Request;

class ProjectImagesController extends Controller
{
    /**
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Project $project)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $projectimages = ProjectImage::where('name_en', 'LIKE', "%$keyword%")
                ->orWhere('name_jp', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('project_id', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $projectimages = ProjectImage::where('project_id', $project->id)
                ->orderBy('sort_order', 'ASC')
                ->paginate($perPage);
        }

        return view('admin.project-images.index', compact('projectimages', 'project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Project $project)
    {
        $statuses = array_combine(ProjectImage::STATUSES, ProjectImage::STATUSES);
        return view('admin.project-images.create', compact('project', 'statuses'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Project $project)
    {
        $requestData = $request->all();
        $this->validate($request, [
            'name_en' => 'required',
            'name_jp' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'project_id' => 'required',
            'status' => 'required'
        ]);

        $image = $request->file('image');
        $name = uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/storage/project-image/');
        $projectImage = $image->move($destinationPath, $name);
        $requestData['image'] = $projectImage->getBasename();

        ProjectImage::create($requestData);

        $url = route('project.image.index', ['project' => $project->id]);
        return redirect($url)->with('flash_message', 'ProjectImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Project $project, $imageId)
    {
        $projectimage = ProjectImage::findOrFail($imageId);

        return view('admin.project-images.show', compact('projectimage', 'project'));
    }

    /**
     * @param Project $project
     * @param $imageId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project, $imageId)
    {
        $projectimage = ProjectImage::findOrFail($imageId);
        $statuses = array_combine(ProjectImage::STATUSES, ProjectImage::STATUSES);

        return view('admin.project-images.edit', compact('projectimage', 'statuses', 'project'));
    }

    /**
     * @param Request $request
     * @param Project $project
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Project $project, ProjectImage $image)
    {
        $this->validate($request, [
            'name_en' => 'required',
            'name_jp' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:5120',
            'project_id' => 'required',
            'status' => 'required'
        ]);

        $requestData = $request->all();

        $requestImage = $request->file('image');
        if ($request->has('image')) {

            $name = uniqid() . '.' . $requestImage->getClientOriginalExtension();
            $destinationPath = public_path('/storage/project-image/');
            $projectImage = $requestImage->move($destinationPath, $name);
            $requestData['image'] = $projectImage->getBasename();
            @unlink(public_path('/storage/project-image/' . $image->image));
        }

        $image->update($requestData);
        $url = route('project.image.index', ['project' => $project->id]);
        return redirect($url)->with('flash_message', 'ProjectImage updated!');
    }

    /**
     * @param Project $project
     * @param $imageId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Project $project, ProjectImage $image)
    {
        @unlink(public_path('/storage/project-image/' . $image->image));
        $image->delete();

        $url = route('project.image.index', ['project' => $project->id]);
        return redirect($url)->with('flash_message', 'ProjectImage deleted!');
    }
}
