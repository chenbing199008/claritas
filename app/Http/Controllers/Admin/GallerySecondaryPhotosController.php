<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GallerySecondaryPhoto;
use Illuminate\Http\Request;

class GallerySecondaryPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, Gallery $gallery)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $gallerysecondaryphotos = GallerySecondaryPhoto::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image_thumb', 'LIKE', "%$keyword%")
                ->orWhere('image_large', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $gallerysecondaryphotos = GallerySecondaryPhoto::where('gallery_id', $gallery->id)
                ->orderBy('sort_order', 'ASC')
                ->paginate($perPage);
        }

        return view('admin.gallery-secondary-photos.index', compact('gallerysecondaryphotos', 'gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Gallery $gallery)
    {
        return view('admin.gallery-secondary-photos.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Gallery $gallery)
    {
        $this->validate($request, [
            'image_thumb' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'image_large' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'gallery_id' => 'required',
        ]);
        $requestData = $request->all();

        $image = $request->file('image_thumb');
        $name = uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/storage/gallery-secondary-image/');
        $projectImage = $image->move($destinationPath, $name);
        $requestData['image_thumb'] = $projectImage->getBasename();

        $image = $request->file('image_large');
        $name = uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/storage/gallery-secondary-image/');
        $projectImage = $image->move($destinationPath, $name);
        $requestData['image_large'] = $projectImage->getBasename();

        GallerySecondaryPhoto::create($requestData);

        $url = route('gallery.secondary.index', ['gallery' => $gallery->id]);
        return redirect($url)->with('flash_message', 'Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Gallery $gallery, $id)
    {
        $gallerysecondaryphoto = GallerySecondaryPhoto::findOrFail($id);

        return view('admin.gallery-secondary-photos.show', compact('gallerysecondaryphoto', 'gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(Gallery $gallery, $id)
    {
        $gallerysecondaryphoto = GallerySecondaryPhoto::findOrFail($id);

        return view('admin.gallery-secondary-photos.edit', compact('gallerysecondaryphoto', 'gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Gallery $gallery, $imageId)
    {
        $image = GallerySecondaryPhoto::findOrFail($imageId);
        $this->validate($request, [
            'image_thumb' => 'image|mimes:jpeg,png,jpg|max:5120',
            'image_large' => 'image|mimes:jpeg,png,jpg|max:5120'
        ]);
        $requestData = $request->all();

        $requestImage = $request->file('image_thumb');
        if ($request->has('image_thumb')) {
            $name = uniqid() . '.' . $requestImage->getClientOriginalExtension();
            $destinationPath = public_path('/storage/gallery-secondary-image/');
            $galleryImage = $requestImage->move($destinationPath, $name);
            $requestData['image_thumb'] = $galleryImage->getBasename();
            @unlink(public_path('/storage/gallery-secondary-image/' . $image->image_thumb));
        }

        $requestImage = $request->file('image_large');
        if ($request->has('image_large')) {
            $name = uniqid() . '.' . $requestImage->getClientOriginalExtension();
            $destinationPath = public_path('/storage/gallery-secondary-image/');
            $galleryImage = $requestImage->move($destinationPath, $name);
            $requestData['image_large'] = $galleryImage->getBasename();
            @unlink(public_path('/storage/gallery-secondary-image/' . $image->imge_large));
        }

        $image->update($requestData);
        $url = route('gallery.secondary.index', ['gallery' => $gallery->id]);
        return redirect($url)->with('flash_message', 'Updated!');
        
//        $gallerysecondaryphoto = GallerySecondaryPhoto::findOrFail($id);
//        $gallerysecondaryphoto->update($requestData);
//
//        return redirect('admin/gallery-secondary-photos')->with('flash_message', 'GallerySecondaryPhoto updated!');
    }

    public function destroy(Gallery $gallery, $imageId)
    {
        $image = GallerySecondaryPhoto::findOrFail($imageId);
        @unlink(public_path('/storage/gallery-secondary-image/' . $image->image_thumb));
        @unlink(public_path('/storage/gallery-secondary-image/' . $image->image_large));
        $image->delete();

        $url = route('gallery.secondary.index', ['gallery' => $gallery->id]);
        return redirect($url)->with('flash_message', 'Deleted!');
    }
}
