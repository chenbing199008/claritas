<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ProjectEventCategory;
use Illuminate\Http\Request;

class ProjectEventCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $projecteventcategories = ProjectEventCategory::where('project_id', 'LIKE', "%$keyword%")
                ->orWhere('event_category_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $projecteventcategories = ProjectEventCategory::latest()->paginate($perPage);
        }

        return view('admin.project-event-categories.index', compact('projecteventcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.project-event-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'project_id' => 'required',
			'event_category_id' => 'required'
		]);
        $requestData = $request->all();
        
        ProjectEventCategory::create($requestData);

        return redirect('admin/project-event-categories')->with('flash_message', 'ProjectEventCategory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $projecteventcategory = ProjectEventCategory::findOrFail($id);

        return view('admin.project-event-categories.show', compact('projecteventcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $projecteventcategory = ProjectEventCategory::findOrFail($id);

        return view('admin.project-event-categories.edit', compact('projecteventcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'project_id' => 'required',
			'event_category_id' => 'required'
		]);
        $requestData = $request->all();
        
        $projecteventcategory = ProjectEventCategory::findOrFail($id);
        $projecteventcategory->update($requestData);

        return redirect('admin/project-event-categories')->with('flash_message', 'ProjectEventCategory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ProjectEventCategory::destroy($id);

        return redirect('admin/project-event-categories')->with('flash_message', 'ProjectEventCategory deleted!');
    }
}
