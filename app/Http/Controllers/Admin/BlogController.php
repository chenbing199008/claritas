<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Blog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blog = Blog::where('title_jp', 'LIKE', "%$keyword%")
                ->orWhere('excerpt_jp', 'LIKE', "%$keyword%")
                ->orWhere('post_jp', 'LIKE', "%$keyword%")
                ->orWhere('posted_date', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $blog = Blog::orderBy('posted_date', 'DESC')->paginate($perPage);
        }

        return view('admin.blog.index', compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $now = Carbon::now()->format('Y-m-d');
        $statuses = array_combine(Blog::STATUSES, Blog::STATUSES);
        return view('admin.blog.create', compact('now', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_jp' => 'required',
            'image_large' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'image_thumb' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'excerpt_jp' => 'required',
            'post_jp' => 'required',
            'posted_date' => 'required',
            'status' => 'required'
		]);

        $requestData = $request->all();

        list ($imageLarge, $imageThumb) = $this->uploadImage($request);
        $requestData['image_large'] = $imageLarge;
        $requestData['image_thumb'] = $imageThumb;
        
        Blog::create($requestData);
        return redirect('/admin/blog')->with('flash_message', 'Blog added!');
    }

    private function uploadImage(Request $request, Blog $blog = null)
    {
        $imageLarge = '';
        $imageThumb = '';
        if ($request->has('image_large')) {
            $image           = $request->file('image_large');
            $name            = uniqid() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/storage/blog-image/');
            $imageLarge      = $image->move($destinationPath, $name)->getBasename();
            @unlink(public_path('/storage/blog-image/' . $blog->image_large));
        }

        if ($request->has('image_thumb')) {
            $image = $request->file('image_thumb');
            $name = uniqid() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/storage/blog-image/');
            $imageThumb = $image->move($destinationPath, $name)->getBasename();
            @unlink(public_path('/storage/blog-image/' . $blog->image_thumb));
        }

        return [$imageLarge, $imageThumb];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blog.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(Blog $blog)
    {
        $statuses = array_combine(Blog::STATUSES, Blog::STATUSES);
        return view('admin.blog.edit', compact('blog', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Blog $blog)
    {
        $this->validate($request, [
            'title_jp' => 'required',
            'excerpt_jp' => 'required',
            'post_jp' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:5120',
            'posted_date' => 'required',
            'status' => 'required'
		]);
        $requestData = $request->all();

        list ($imageLarge, $imageThumb) = $this->uploadImage($request, $blog);
        if ($imageLarge) {
            $requestData['image_large'] = $imageLarge;
        }
        if ($imageThumb) {
            $requestData['image_thumb'] = $imageThumb;
        }

        $blog->update($requestData);

        return redirect('/admin/blog')->with('flash_message', 'Blog updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Blog $blog)
    {
        @unlink(public_path('/storage/blog-image/' . $blog->image_large));
        @unlink(public_path('/storage/blog-image/' . $blog->image_thumb));

        $blog->delete();
        return redirect('/admin/blog')->with('flash_message', 'Blog deleted!');
    }
}
