<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

class NewsController extends Controller
{

    public function create()
    {
        return view('admin.news.create', ['now' => date('Y-m-d', time())]);
    }

    public function index(Request $request)
    {
        $perPage = 20;

        $news = News::orderBy('published_at', 'desc')->paginate($perPage);

        if (!$news) {
            return view('admin.news.index', compact('news'));
        }

        foreach ($news as $index => $item) {
            $item->cutTitle = mb_substr($item->title, 0, 20);
            $item->cutBody = strip_tags(mb_substr($item->body, 0, 20));
            $item->hashTags = explode(',', $item->hash_tags);
        }

        return view('admin.news.index', compact('news'));
    }

    public function show($id)
    {
        $news = News::findOrFail($id);

        $news->hashTags = explode(',', $news->hash_tags);
        return view('admin.news.show', compact('news'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'hash_tags' => 'required',
            'published_at' => 'required',
        ]);
        $requestData = $request->all();

        if (isset($requestData['s'])) {
            unset($requestData['s']);
        }
        News::create($requestData);

        return redirect('admin/news')->with('flash_message', 'News added!');
    }

    public function edit($id)
    {
        $news = News::findOrFail($id);
//        $news->body = nl2br($news->body);
        return view('admin.news.edit', compact('news'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'hash_tags' => 'required',
            'published_at' => 'required',
        ]);
        $requestData = $request->all();
        $requestData['body'] = trim($requestData['body']);

//        var_dump($requestData['body']);
//        dd($requestData['body']);

        if (isset($requestData['s'])) {
            unset($requestData['s']);
        }
        $news = News::findOrFail($id);
        $news->update($requestData);

        return redirect('admin/news')->with('flash_message', 'News updated!');
    }


    public function destroy($id)
    {
        $noOfProject = News::where('id', $id)->count();
        if (!$noOfProject) {
            return redirect('admin/news')->with('flash_error', 'Sorry. Cant delete news because it has projects');
        }

        News::destroy($id);

        return redirect('admin/news')->with('flash_message', 'News deleted!');
    }



}
