<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\Category;
use App\Country;
use App\EventCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;
use App\ProjectCategory;
use App\ProjectEventCategory;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $project = Project::select('projects.*')
                ->join('countries', 'countries.id', '=', 'projects.country_id')
                ->join('areas', 'areas.id', '=', 'countries.area_id')
                ->where('projects.name_en', 'LIKE', "%$keyword%")
                ->orWhere('projects.name_jp', 'LIKE', "%$keyword%")
                ->orWhere('countries.name_en', 'LIKE', "%$keyword%")
                ->orWhere('countries.name_jp', 'LIKE', "%$keyword%")
                ->orWhere('areas.name_en', 'LIKE', "%$keyword%")
                ->orWhere('areas.name_jp', 'LIKE', "%$keyword%")
                ->orWhere('projects.country_id', 'LIKE', "%$keyword%")
                ->orWhere('projects.year', 'LIKE', "%$keyword%")
                ->orWhere('projects.address', 'LIKE', "%$keyword%")
                ->orWhere('projects.status', 'LIKE', "%$keyword%")
                ->orderBy('projects.year', 'ASC')->paginate($perPage);
        } else {
            $project = Project::orderBy('year', 'DESC')
                ->orderBy('sort_order', 'ASC')
                ->orderBy('name_en', 'ASC')
                ->with('country')->paginate($perPage);
        }

        return view('admin.project.index', compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $statuses = array_combine(Blog::STATUSES, Blog::STATUSES);
        $countries = Country::orderBy('name_en', 'ASC')->pluck('name_en', 'id')->toArray();
        $categories = Category::orderBy('sort_order')->pluck('name_en', 'id')->toArray();
        foreach ($categories as $i => $category) {
            $categories[$i] = "($i) $category";
        }
        $eventCategories = EventCategory::orderBy('sort_order')->pluck('name_en', 'id')->toArray();
        foreach ($eventCategories as $i => $category) {
            $eventCategories[$i] = "($i) $category";
        }

        return view('admin.project.create', compact('countries', 'statuses', 'categories', 'eventCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_en' => 'required',
			'name_jp' => 'required',
			'country_id' => 'required',
			'year' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $project = Project::create($requestData);

        $categories = $requestData['categories'] ?? [];
        foreach ($categories as $category) {
            ProjectCategory::create([
                'category_id' => $category,
                'project_id' => $project->id
            ]);
        }

        $eventCategories = $requestData['event_category'] ?? [];
        foreach ($eventCategories as $category) {
            ProjectEventCategory::create([
                'event_category_id' => $category,
                'project_id' => $project->id
            ]);
        }

        return redirect('admin/project')->with('flash_message', 'Project added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);

        return view('admin.project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(Project $project)
    {
        $statuses = array_combine(Blog::STATUSES, Blog::STATUSES);
        $countries = Country::orderBy('name_en', 'ASC')->pluck('name_en', 'id')->toArray();
        $categories = Category::orderBy('sort_order')->pluck('name_en', 'id')->toArray();
        foreach ($categories as $i => $category) {
            $categories[$i] = "($i) $category";
        }
        $eventCategories = EventCategory::orderBy('sort_order')->pluck('name_en', 'id')->toArray();
        foreach ($eventCategories as $i => $category) {
            $eventCategories[$i] = "($i) $category";
        }

        return view('admin.project.edit', compact('project', 'countries', 'statuses', 'categories', 'eventCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_en' => 'required',
			'name_jp' => 'required',
			'country_id' => 'required',
			'year' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        $categories = $requestData['categories'] ?? [];
        
        $project = Project::findOrFail($id);
        $project->update($requestData);

        ProjectCategory::where('project_id', $project->id)->delete();
        foreach ($categories as $category) {
            ProjectCategory::create([
                'category_id' => $category,
                'project_id' => $project->id
            ]);
        }

        ProjectEventCategory::where('project_id', $project->id)->delete();
        $eventCategories = $requestData['event_category'] ?? [];
        foreach ($eventCategories as $category) {
            ProjectEventCategory::create([
                'event_category_id' => $category,
                'project_id' => $project->id
            ]);
        }

        return redirect('admin/project')->with('flash_message', 'Project updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Project::destroy($id);

        return redirect('admin/project')->with('flash_message', 'Project deleted!');
    }
}
