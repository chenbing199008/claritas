<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\EventCategory;
use Illuminate\Http\Request;

class EventCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $eventcategories = EventCategory::where('name_en', 'LIKE', "%$keyword%")
                ->orWhere('name_jp', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $eventcategories = EventCategory::orderBy('sort_order', 'ASC')->paginate($perPage);
        }

        return view('admin.event-categories.index', compact('eventcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.event-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_en' => 'required',
			'name_jp' => 'required'
		]);
        $requestData = $request->all();
        
        EventCategory::create($requestData);

        return redirect('admin/event-categories')->with('flash_message', 'EventCategory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $eventcategory = EventCategory::findOrFail($id);

        return view('admin.event-categories.show', compact('eventcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $eventcategory = EventCategory::findOrFail($id);

        return view('admin.event-categories.edit', compact('eventcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_en' => 'required',
			'name_jp' => 'required'
		]);
        $requestData = $request->all();
        
        $eventcategory = EventCategory::findOrFail($id);
        $eventcategory->update($requestData);

        return redirect('admin/event-categories')->with('flash_message', 'EventCategory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        EventCategory::destroy($id);

        return redirect('admin/event-categories')->with('flash_message', 'EventCategory deleted!');
    }
}
