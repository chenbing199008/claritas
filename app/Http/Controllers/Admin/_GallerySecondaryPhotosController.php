<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GallerySecondaryPhoto;
use Illuminate\Http\Request;

class _GallerySecondaryPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $gallerysecondaryphotos = GallerySecondaryPhoto::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image_thumb', 'LIKE', "%$keyword%")
                ->orWhere('image_large', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $gallerysecondaryphotos = GallerySecondaryPhoto::latest()->paginate($perPage);
        }

        return view('admin.gallery-secondary-photos.index', compact('gallerysecondaryphotos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.gallery-secondary-photos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image_thumb' => 'required',
			'image_large' => 'required'
		]);
        $requestData = $request->all();
        
        GallerySecondaryPhoto::create($requestData);

        return redirect('admin/gallery-secondary-photos')->with('flash_message', 'GallerySecondaryPhoto added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $gallerysecondaryphoto = GallerySecondaryPhoto::findOrFail($id);

        return view('admin.gallery-secondary-photos.show', compact('gallerysecondaryphoto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $gallerysecondaryphoto = GallerySecondaryPhoto::findOrFail($id);

        return view('admin.gallery-secondary-photos.edit', compact('gallerysecondaryphoto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'image_thumb' => 'required',
			'image_large' => 'required'
		]);
        $requestData = $request->all();
        
        $gallerysecondaryphoto = GallerySecondaryPhoto::findOrFail($id);
        $gallerysecondaryphoto->update($requestData);

        return redirect('admin/gallery-secondary-photos')->with('flash_message', 'GallerySecondaryPhoto updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        GallerySecondaryPhoto::destroy($id);

        return redirect('admin/gallery-secondary-photos')->with('flash_message', 'GallerySecondaryPhoto deleted!');
    }
}
