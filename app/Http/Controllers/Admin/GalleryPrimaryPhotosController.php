<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GalleryPrimaryPhoto;
use Illuminate\Http\Request;

class GalleryPrimaryPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, Gallery $gallery)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $galleryprimaryphotos = GalleryPrimaryPhoto::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image_thumb', 'LIKE', "%$keyword%")
                ->orWhere('image_large', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $galleryprimaryphotos = GalleryPrimaryPhoto::where('gallery_id', $gallery->id)
                ->orderBy('sort_order', 'ASC')
                ->paginate($perPage);
        }


        return view('admin.gallery-primary-photos.index', compact('galleryprimaryphotos', 'gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Gallery $gallery)
    {
        return view('admin.gallery-primary-photos.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Gallery $gallery)
    {
        $this->validate($request, [
            'image_thumb' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'image_large' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'gallery_id' => 'required',
        ]);
        $requestData = $request->all();

        $image = $request->file('image_thumb');
        $name = uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/storage/gallery-primary-image/');
        $projectImage = $image->move($destinationPath, $name);
        $requestData['image_thumb'] = $projectImage->getBasename();

        $image = $request->file('image_large');
        $name = uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/storage/gallery-primary-image/');
        $projectImage = $image->move($destinationPath, $name);
        $requestData['image_large'] = $projectImage->getBasename();

        GalleryPrimaryPhoto::create($requestData);

        $url = route('gallery.primary.index', ['gallery' => $gallery->id]);
        return redirect($url)->with('flash_message', 'Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Gallery $gallery, $id)
    {
        $galleryprimaryphoto = GalleryPrimaryPhoto::findOrFail($id);

        return view('admin.gallery-primary-photos.show', compact('galleryprimaryphoto', 'gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(Gallery $gallery, $id)
    {
        $galleryprimaryphoto = GalleryPrimaryPhoto::findOrFail($id);

        return view('admin.gallery-primary-photos.edit', compact('galleryprimaryphoto', 'gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Gallery $gallery, $imageId)
    {
        $image = GalleryPrimaryPhoto::findOrFail($imageId);
        $this->validate($request, [
            'image_thumb' => 'image|mimes:jpeg,png,jpg|max:5120',
            'image_large' => 'image|mimes:jpeg,png,jpg|max:5120'
        ]);
        $requestData = $request->all();

        $requestImage = $request->file('image_thumb');
        if ($request->has('image_thumb')) {
            $name = uniqid() . '.' . $requestImage->getClientOriginalExtension();
            $destinationPath = public_path('/storage/gallery-primary-image/');
            $galleryImage = $requestImage->move($destinationPath, $name);
            $requestData['image_thumb'] = $galleryImage->getBasename();
            @unlink(public_path('/storage/gallery-primary-image/' . $image->image_thumb));
        }

        $requestImage = $request->file('image_large');
        if ($request->has('image_large')) {
            $name = uniqid() . '.' . $requestImage->getClientOriginalExtension();
            $destinationPath = public_path('/storage/gallery-primary-image/');
            $galleryImage = $requestImage->move($destinationPath, $name);
            $requestData['image_large'] = $galleryImage->getBasename();
            @unlink(public_path('/storage/gallery-primary-image/' . $image->imge_large));
        }

        $image->update($requestData);
        $url = route('gallery.primary.index', ['gallery' => $gallery->id]);
        return redirect($url)->with('flash_message', 'Updated!');
        
//        $galleryprimaryphoto = GalleryPrimaryPhoto::findOrFail($id);
//        $galleryprimaryphoto->update($requestData);
//
//        return redirect('admin/gallery-primary-photos')->with('flash_message', 'GalleryPrimaryPhoto updated!');
    }

    public function destroy(Gallery $gallery, $imageId)
    {
        $image = GalleryPrimaryPhoto::findOrFail($imageId);
        @unlink(public_path('/storage/gallery-primary-image/' . $image->image_thumb));
        @unlink(public_path('/storage/gallery-primary-image/' . $image->image_large));
        $image->delete();

        $url = route('gallery.primary.index', ['gallery' => $gallery->id]);
        return redirect($url)->with('flash_message', 'Deleted!');
    }
}
