<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Illuminate\Support\Facades\Cookie;

class GetPreferredLanguage extends Middleware
{
    public function handle($request, Closure $next)
    {
        $acceptLang = ['ja', 'en'];
        $langInUrl = $request->segment(1);

        if ($langInUrl && in_array($langInUrl, $acceptLang)) {
            // Lang in url is given 1st priority
            Cookie::queue(Cookie::make('lang', $langInUrl, 60));
        }
        if ($langInUrl === "en") {
            return redirect(str_replace('/en', '', $request->getRequestUri()));
        }

        if ($langInUrl && in_array($langInUrl, $acceptLang)) {
            $lang = $langInUrl;
        } else {
            if (Cookie::has('lang')) {
                $langInCookie = Cookie::get('lang');
                $lang = $langInCookie;
            } else {
                $langFromBrowserSetting = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? '', 0, 2);
                $lang = in_array($langFromBrowserSetting, $acceptLang) ? $langFromBrowserSetting : 'en';
            }
            if ($lang === "ja") {
                $url = "ja" . $request->getRequestUri();
                return redirect($url);
            }
        }

        app()->setLocale($lang);
        Cookie::queue(Cookie::make('lang', $lang, 60));

        if ($lang === "ja") {
            $lang = "jp";
        }
        if ($lang === 'en') {
            $url = url('ja' . $request->getRequestUri());
        } else {
//            $url = url('en' . $request->getRequestUri());
            $url = url(str_replace('/ja', '/en', $request->getRequestUri()));
        }
        define('LANG_FIELD', $lang);
        define('ALTERNATIVE_LANG_PAGE', $url);
        return $next($request);
    }
}
