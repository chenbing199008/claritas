## Create Zip
```
rm -f claritas.zip && tar --exclude='./idea' --exclude='.git' --exclude='bootstrap/cache/'  -zcvf ./claritas.zip .
```

## Move zip to the server
```
sudo scp -i ~/.ssh/id_rsa -r ~/bikal/claritas/claritas.zip root@209.97.184.43:~/claritas.zip
```

## ssh into the server
```
ssh root@209.97.184.43
```

## Commands to run in the server
```
mv claritas old-claritas && \
mkdir claritas && tar -xvzf claritas.zip -C claritas && \
cd claritas && docker-compose up -d --build && \
mkdir -p bootstrap/cache && \
sudo chown -R www-data:www-data ./ && \
chmod -R 775 storage && \
chmod -R 775 bootstrap/cache
```

## BackOffice
```
http://localhost/admin
u: admin@claritas.com
p: admin@123
```

Other Commands to run on local
```
chmod -R 775 storage/logs
chmod -R 775 storage/framework/sessions 
chmod -R 775 storage/framework/views
```


## SASS
sass --watch --source-map resources/sass/frontend.scss public/css/main.min.css


## other command
docker exec -it claritas-web bash

composer install && \
chown -R www-data:www-data ./ && \
chmod -R 775 storage && \
chmod -R 775 bootstrap/cache && \
php artisan cache:clear
