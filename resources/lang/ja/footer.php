<?php

$year = (new \DateTime())->format('Y');
return [
    'column1' => [
        'office_name' => '東京オフィス',
        'address_1' => '〒153-0051 東京都目黒区上目黒 3-17-1 Bark Lane 中目黒 008',
        'address_2' => '',
        'phone' => '+81 (0) 3 3710 8755',
        'email' => 'japan@claritas-marketing.com'
    ],
    'column2' => [
        'office_name' => 'ロンドンオフィス',
        'address_1' => '6 Aztec Row, Bernes Road, London N1 0PW',
        'address_2' => 'United Kingdom',
        'phone' => '+44 (0) 20 7794 1123',
        'email' => 'info@claritas-marketing.com'
    ],
    'copyright' => "Copyright © $year Claritas Marketing Ltd. All Rights Reserved."
];
