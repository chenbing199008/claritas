<?php

return [
    'right' => [
        'business_name' => '株式会社クラリタスマーケティング',
        'address_1' => '〒153-0051 東京都目黒区上目黒 3-17-1 Bark Lane 中目黒 008',
        'address_2' => '008 Bark Lane Nakameguro, 3-17-1 Kamimeguro, Meguro-ku, Tokyo 153-0051'
    ],

    'send' => '送信',
    'errors' => [
        'name' => '氏名を入力してください。',
        'companyName' => "会社名を入力してください。",
        'tel' => "電話番号を入力してください。",
        'email' => "メールアドレスを入力してください。",
        'message' => "お問い合わせ内容を入力してください。",
    ],

    'placeholder' => [
        'name' => 'お名前',
        'companyName' => "会社名",
        'tel' => "電話番号",
        'email' => "メールアドレス",
        'message' => "お問い合わせ内容",
    ],

    'sentEmailSuccess' => [
        'title' => 'メッセージ送信完了',
        'parts' => [
            'お問い合わせいただき、ありがとうございます。',
            '確認のため、自動返信メールをお送りいたします。',
            'お問い合わせをいただいた内容については、確認の上ご返信させていただきます。',
        ]
    ]
];
