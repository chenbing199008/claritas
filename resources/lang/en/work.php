<?php

return [
    'title' => 'We provide worldwide support',
    'paragraph1' => "With offices in London and Tokyo, our reach is worldwide. We have managed events and projects in several areas such as in Europe, Russia, Middle East, Africa, America, and in Asia. We've been working in London, a place famous for its diversity and multiculturalism, for many years. We can truly support your business with a global perspective that we've developed in this environment.",
    'paragraph2' => "Our work and coverage around the world can be found on the map below.",
    'contact_us' => 'Contact Us',
    'choose_office' => 'Please choose the office',
    'london_office' => 'London Office',
    'tokyo_office' => 'Tokyo Office',
];
