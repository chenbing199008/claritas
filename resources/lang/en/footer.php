<?php

$year = (new \DateTime())->format('Y');
return [
    'column1' => [
        'office_name' => 'London Office',
        'address_1' => '6 Aztec Row, Berners Road, London N1 0PW',
        'address_2' => 'United Kingdom',
        'phone' => '+44 (0) 20 7794 1123',
        'email' => 'info@claritas-marketing.com'
    ],
    'column2' => [
        'office_name' => 'Tokyo Office',
        'address_1' => '008 Bark Lane Nakameguro, 3-17-1 Kamimeguro, Meguro-ku, Tokyo 153-0051',
        'address_2' => 'Japan',
        'phone' => '+81 (0)3 3710 8755',
        'email' => 'japan@claritas-marketing.com'
    ],
    'copyright' => "Copyright © $year Claritas Marketing Ltd. All Rights Reserved."
];
