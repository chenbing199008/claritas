<?php

return [
    'right' => [
        'business_name' => 'Claritas Marketing Co., Ltd.',
        'address_1' => '008 Bark Lane Nakameguro, 3-17-1 Kamimeguro, Meguro-ku, Tokyo 153-0051',
        'address_2' => 'Japan',
    ],

    'send' => 'SEND',

    'errors' => [
        'name' => 'Please provide your name.',
        'companyName' => "Please provide your company's name.",
        'tel' => "Please provide your phone number.",
        'email' => "Please provide your email address.",
        'message' => "Please tell us what you would like to discuss with us.",
    ],

    'placeholder' => [
        'name' => 'Name',
        'companyName' => "Company Name",
        'tel' => "Tel",
        'email' => "Email",
        'message' => "Message",
    ],

    'sentEmailSuccess' => [
        'title' => 'Your contact form has been sent successfully.',
        'parts' => [
            'Thank you for contacting us.',
            'We will come back to you as soon as possible.',
        ]
    ]
];
