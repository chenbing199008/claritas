<?php

return [
    'home' => 'home',
    'work' => 'PROJECTS',
    'gallery' => 'gallery',
    'photo_blog' => 'photo blog',
    'contact' => 'contact',
    'industry_news' => 'INDUSTRY NEWS',
    'privacy_policy' => 'PRIVACY POLICY'
];
