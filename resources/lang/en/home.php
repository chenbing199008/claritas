<?php

return [
    'hero-text' => 'We value your moment.',
    'about1' => [
        'title' => 'Providing global consultation for your events and projects',
        'description' => 'Claritas Marketing offers a one-stop solution for all your event and marketing needs, from stand design, construction, outsourcing staff and catering, to graphic design, digital content and more. We strive to deliver excellence and to make our clients’ vision a reality.',
    ],

    'meta' => 'We are event specialists based in the UK and Japan, offering a one-stop solution for your event and marketing needs, from stand design to construction, operation and more.',


    'about2' => [
        'title' => 'We are event experts',
        'description' => '<p>For more than 20 years, Claritas Marketing has worked with a diverse range of clients to deliver events spanning
exhibitions, business receptions, to seminars and symposiums. Through our offices in London and Tokyo we offer
seamless interconnected services to support clients at every stage, using our extensive global network.</p>
                            <p>Our cross-cultural and experienced international teams understand local practices in both Europe and in Japan.
We use our expertise to plan and deliver a targeted approach for each destination, befitting the local context.</p>'

    ],
    'service' => [
        'title' => 'Our services',
        'description' => '<p>A successful event requires more than just execution. Planning is an equally critical component. At
Claritas Marketing we approach every step with consideration and coordination, to deliver
exceptional client experiences.</p>',
        'service1' => [
            'title' => 'EVENT MANAGEMENT',
            'description' => '<p>At Claritas Marketing we create bespoke plans for our clients based on our intercultural expertise to
deliver projects that generate successful outcomes. We manage all aspects of events, from hiring
venues, arranging guest speakers to providing specialised catering and more. We build trust through
meticulous communication and ensuring we meet the expectations of every stakeholder.</p>'
        ],
        'service2' => [
            'title' => 'STAND CONSTRUCTION',
            'description' => ' <p>Our dedicated teams of professionals oversee an end-to-end event planning and execution process,
from exhibition booth construction to venue styling. We commit to excellence in every element with
speed to delivery.</p>'
        ],
        'service3' => [
            'title' => 'DESIGN',
            'description' => "<p>From graphics to spatial, we believe in the power of compelling design. At Claritas Marketing we
employ design as a critical tool for communication with an aim to creatively express our clients'
vision. With Claritas Marketing you can project your ideas cross-culturally with confidence. We are
here to ensure your message is conveyed accurately and effectively.</p>",
        ],
        'service4' => [
            'title' => 'MEDIA BUYING',
            'description' => '<p>Media consumption and trends differ significantly across countries. At Claritas Marketing, we design
and deliver plans specific to the target audience, individually tailored to the clients’ portfolio. From
online to traditional media, we offer extensive media planning across every platform.</p>'
        ],
        'service5' => [
            'title' => 'PUBLIC RELATIONS',
            'description' => '<p>Effective public relations not only generates interest but also builds credibility for your business. We
offer comprehensive PR management based on our in-depth understanding of local media
landscape. Our dedicated PR team create and manage effective campaigns through press events,
content creation, collaboration with local journalists/influencers and continuous media engagement.</p>'
        ]
    ]
];
