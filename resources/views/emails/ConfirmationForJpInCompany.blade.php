<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
</head>

<body>
    <div>お問合せ内容を確認してください：</div>

    <p class="mb-5 mt-5">--------------------------------------------------------------</p>

    <div>
        <div>■ お問い合わせオフィス {{ $info['office'] == 1 ? '東京オフィス' : 'ロンドンオフィス' }}</div>
        <div>■ お名前 {{ $info['name'] }} </div>
        <div>■ 会社名 {{ $info['companyName'] }}</div>
        <div>■ 電話番号 {{ $info['tel'] }}</div>
        <div>■ メールアドレス {{ $info['email'] }}</div>
        <div style="margin-bottom: 15px">■ お問い合わせ内容 </div>
        <div>{!! nl2br($info['message'])  !!}</div>
    </div>

</body>

</html>