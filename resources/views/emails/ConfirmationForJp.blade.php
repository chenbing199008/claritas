<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
</head>

<body>
    <div style="margin-bottom: 15px">{{$info['name']}} 様</div>
    <div>お問い合わせありがとうございます。</div>
    <div>下記の通りお問い合わせを受け付けました。</div>
    <div>確認次第ご連絡いたしますので、 少々お待ちください。</div>

    <p class="mb-5 mt-5">--------------------------------------------------------------</p>

    <div>
        <div>■ お問い合わせオフィス {{ $info['office'] == 1 ? '東京オフィス' : 'ロンドンオフィス' }}</div>
        <div>■ お名前 {{ $info['name'] }} </div>
        <div>■ 会社名 {{ $info['companyName'] }}</div>
        <div>■ 電話番号 {{ $info['tel'] }}</div>
        <div>■ メールアドレス {{ $info['email'] }}</div>
        <div style="margin-bottom: 15px">■ お問い合わせ内容 </div>
        <div>{!! nl2br($info['message'])  !!}</div>
    </div>

    <div class="mb-5 mt-5">--------------------------------------------------------------</div>
    <img src="{{ $baseUrl .'/mini-img/logo_navbar_sm.png'}}" alt="logo" style="margin-bottom: 15px;margin-top: 15px">
    <div style="font-weight: bolder;margin-bottom: 15px">東京オフィス</div>
    <div>株式会社クラリタスマーケティング</div>
    <div>〒153-0051 東京都目黒区上目黒 3-17-1 Bark Lane 中目黒 008</div>
    <div>008 Bark Lane Nakameguro, 3-17-1 Kamimeguro, Meguro-ku, Tokyo 1 53-0051</div>
    <div style="margin-bottom: 15px">TEL: +81 (0)3 3710 8755 / FAX: +81 (0)3 6451 0812</div>

    <div style="font-weight: bolder;">ロンドンオフィス</div>
    <div>Claritas Marketing Ltd.</div>
    <div>6 Aztec Row, Berners Road, London N1 0PW</div>
    <div>United Kingdom</div>
    <div>+44 (0) 20 7794 1123</div>

</body>

</html>