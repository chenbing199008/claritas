<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
</head>

<body>
    <div style="margin-bottom: 15px">Dear {{ $info['name'] }},</div>
    <div>Thank you for contacting Claritas Marketing Ltd.</div>
    <div> We have received your inquiry and will respond to you very soon.</div>

    <p class="mb-5 mt-5">--------------------------------------------------------------</p>

    <div>
        <div>■ Office location: {{ $info['office'] == 1 ? 'Tokyo Office' : 'London Office' }}</div>
        <div>■ Name: {{ $info['name'] }}</div>
        <div>■ Company name: {{ $info['companyName'] }}</div>
        <div>■ Tel: {{ $info['tel'] }}</div>
        <div>■ Email: {{ $info['email'] }}</div>
        <div style="margin-bottom: 15px">■ Your message：</div>
        <div>{!! nl2br($info['message'])  !!}</div>
    </div>

    <div class="mb-5 mt-5">--------------------------------------------------------------</div>
    <img src="{{ $baseUrl .'/mini-img/logo_navbar_sm.png'}}" alt="logo" style="margin-bottom: 15px;margin-top: 15px">
    <div style="font-weight: bolder;margin-bottom: 15px">Claritas Marketing Ltd.</div>
    <div style="font-weight: bolder;">LONDON office</div>
    <div>6 Aztec Row, Berners Road, London N1 0PW United Kingdom</div>
    <div style="margin-bottom: 15px">+44 (0) 20 7794 1123</div>
    <div style="font-weight: bolder;">TOKYO office</div>
    <div>Claritas Marketing Co., Ltd</div>
    <div>008 Bark Lane Nakameguro, 3-17-1 Kamimeguro, Meguro-ku, Tokyo 153-0051 Japan</div>
    <div>TEL: +81 (0)3 3710 8755 / FAX: +81 (0)3 6451 0812</div>

</body>

</html>