<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
</head>

<body>
    <div>Please see the enquiry below:</div>

    <p class="mb-5 mt-5">--------------------------------------------------------------</p>

    <div>
        <div>■ Office location: {{ $info['office'] == 1 ? 'Tokyo Office' : 'London Office' }}</div>
        <div>■ Name: {{ $info['name'] }}</div>
        <div>■ Company name: {{ $info['companyName'] }}</div>
        <div>■ Tel: {{ $info['tel'] }}</div>
        <div>■ Email: {{ $info['email'] }}</div>
        <div style="margin-bottom: 15px">■ message：</div>
        <div>{!! nl2br($info['message'])  !!}</div>
    </div>
</body>

</html>