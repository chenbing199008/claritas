<!-- Footer section -->
<footer class="main-footer-home">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-2">
                <a href="/{{ app()->getLocale() }}" class="footer-logo">
                    <figure><img src="{{ asset('mini-img/logo_footer.png') }}" alt="Claritas"></figure>
                    {{--<figure><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/2f1674718ca720fe0916b0b743d67d76.png" alt="Claritas"></figure>--}}
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <ul class="footer-infos">
                    <li>{{ __('footer.column1.office_name') }}</li>
                    <li>{{ __('footer.column1.address_1') }}</li>
                    <li>{{ __('footer.column1.address_2') }}</li>
                    <li><a href="tel:{{ __('footer.column1.phone')  }}">{{ __('footer.column1.phone') }}</a></li>
                </ul>
            </div>

            <div class="col-12 col-sm-6 col-md-3">
                <ul class="footer-infos">
                    <li>{{ __('footer.column2.office_name') }}</li>
                    <li>{{ __('footer.column2.address_1') }}</li>
                    <li>{{ __('footer.column2.address_2') }}</li>
                    <li><a href="tel:{{ __('footer.column2.phone')  }}">{{ __('footer.column2.phone') }}</a></li>
                </ul>
            </div>
        </div>
        <p class="copyright-text">{{ __('footer.copyright')  }}</p>
    </div>
</footer>
<!-- /Footer section -->
