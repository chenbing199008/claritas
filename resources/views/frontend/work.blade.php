@extends('layouts.app')

@section('content')
    <div id="main-wrapper">
    @include('frontend.menu')

        <!-- Work with map section -->
        <div class="work-map-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="work-map-top-info">
                            <h2>{{ __('work.title') }}</h2>
                            <p>{{ __('work.paragraph1') }}</p>
                            <p>{{ __('work.paragraph2') }}</p>
                        </div>
                    </div>
                </div>

                <div id="world-map" class="world-map">
                    <img src="{{ url('img/area/base.svg') }}" usemap="#image-map" class="world-map-image" default-width="695" default-height="325">
                    <map name="image-map"></map>
                </div>
            </div>
        </div>
        <!-- /Work with map section -->

        <!-- Country select form -->
        <div class="country-select-form">
            <div class="container">
                <div class="country-select-wrap">
                    <div class="form-group">
                        <select id="area-select" class="form-control">
                            <option value="">{{ __('Select an Area') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="country-select" class="form-control">
                            <option value="">{{ __('Select a Country') }}</option>
                        </select>
                    </div>
                </div>
                <div class="project-listings">
                    <table id="project-table" class="project-table table table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('Project Name') }}</th>
                            <th>{{ __('Country') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($projects as $project)
                        <tr class="clickable"
                            data-country="{{ $project['country_code'] }}"
                            data-area="{{ $project['area_code'] }}"
                            data-toggle="collapse"
                            data-target="#collapseProject{{ $project['id'] }}"
                            aria-expanded="false"
                            aria-controls="collapseProject{{ $project['id'] }}"
                        >
                            <td class="title">
                                <i class="fal fa-chevron-down"></i>
                                {{ $project['name'] }}
                            </td>
                            <td>{{ $project['country'] }}</td>
                        </tr>
                        <tr class="project-collapse-wrap"
                            data-country="{{ $project['country_code'] }}"
                            data-area="{{ $project['area_code'] }}"
                        >
                            <td colspan="2">
                                <div class="collapse" id="collapseProject{{ $project['id'] }}" data-parent='#project-table'>
                                    <div class="card card-body">
                                        @if( $project['event_category_free_description'])
                                            <ul class="project-event-categories">
                                                {{ $project['event_category_free_description'] }}
                                            </ul>
                                        @else
                                        <ul class="project-event-categories">
                                            @foreach ($project['eventCategories'] as $category)
                                                <li>{{ $category }}</li>
                                            @endforeach
                                        </ul>
                                        @endif
                                        <p class="project-date">{{ $project['year'] }}</p>
                                        <ul class="project-providing">
                                            @foreach ($project['services'] as $service)
                                                <li>{{ $service }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /Country select form -->

        @include('frontend.footer')

        <!-- Project Modal -->
        <div id="project-modal" class="modal fade dark-modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                    </div>
                    <div class="modal-body">
                        <div class="project-container"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Project Modal -->
    </div>
@endsection
