@extends('layouts.app')

@section('content')
    <div id="main-wrapper">
        @include('frontend.menu')
        <div style="display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 85vh;">
            <div class="container p-5">
                <h2 class="mb-4">{{__('contact.sentEmailSuccess.title')}}</h2>
                @foreach(__('contact.sentEmailSuccess.parts') as $part)
                    <div>{{ $part }}</div>
                @endforeach
            </div>

            <!-- Footer section -->
            <footer class="main-footer">
                <div class="container">
                    <p class="copyright-text">{{ __('footer.copyright') }}</p>
                </div>
            </footer>
        </div>

        <!-- /Footer section -->
    </div>
@endsection
