<!-- Navbar section -->
<header class="main-navigation">
    <nav class="navbar navbar-expand-cb">
        <div class="col-4" style="position: static;white-space: nowrap;">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="bt-menu-trigger">
                    <span></span>
                </div>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item d-lg-none {{ $menu === 'index' ? 'active' : '' }}">
                        <a class="nav-link" href="/{{ app()->getLocale() === "en" ? '' : app()->getLocale() . '/' }}">
                            {{ __('menu.home') }}
                            <span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item {{ $menu === 'work' ? 'active' : '' }}">
                        <a class="nav-link" href="/{{ app()->getLocale() === "en" ? '' : app()->getLocale() . '/' }}work">
                            {{ __('menu.work') }}<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item {{ $menu === 'gallery' ? 'active' : '' }}">
                        <a class="nav-link" href="/{{ app()->getLocale() === "en" ? '' : app()->getLocale() . '/' }}gallery">
                            {{ __('menu.gallery') }}<span class="sr-only">(current)</span>
                        </a>
                    </li>

                    @if(app()->getLocale() === "ja")
                        <li class="nav-item {{ $menu === 'industry_news' ? 'active' : '' }}">
                            <a class="nav-link" href="/{{ app()->getLocale() === "en" ? '' : app()->getLocale() . '/' }}industry_news">
                                {{ __('menu.industry_news') }}
                            </a>
                        </li>
                    @endif

                    <li class="nav-item {{ $menu === 'contact' ? 'active' : '' }}">
                        <a class="nav-link"
                           href="/{{ app()->getLocale() === "en" ? '' : app()->getLocale() . '/' }}contact"
                        >{{ __('menu.contact') }}</a>
                    </li>



                    @if(app()->getLocale() === "ja")
                        <li class="nav-item {{ $menu === 'privacy_policy' ? 'active' : '' }}">
                            <a class="nav-link" href="/{{ app()->getLocale() === "en" ? '' : app()->getLocale() . '/' }}privacy">
                            {{ __('menu.privacy_policy') }}
                            </a>
                        </li>
                    @endif

                </ul>
            </div>
        </div>
        <a class="navbar-brand" href="/{{ app()->getLocale() === 'en' ? '' : app()->getLocale() }}">
            <figure class="d-none d-sm-block"><img src="{{ asset('mini-img/logo_navbar.png') }}" alt="Claritas"></figure>
            <figure class="d-block d-sm-none"><img src="{{ asset('mini-img/logo_navbar_mb.png') }}" alt="Claritas"></figure>
        </a>
        <div class="col-4 language-status">
            <a class="language-item {{ app()->getLocale() === 'ja' ? 'language-active' : ''  }}"
               href="{{ app()->getLocale() === 'en' ? ALTERNATIVE_LANG_PAGE : '#'  }}"
            >{{__('JP') }}</a>
            <a class="language-item {{ app()->getLocale() === 'en' ? 'language-active' : '' }}"
               href="{{ app()->getLocale() === 'ja' ? ALTERNATIVE_LANG_PAGE : '#'  }}"
            >EN</a>
        </div>
    </nav>
</header>
<!-- /Navbar section -->
