@extends('layouts.app')
@section('content')
    <div id="main-wrapper">
    @include('frontend.menu')
        <!-- Photo blog section -->
        <div class="photo-blog-section">
            <div class="row no-gutters">
                @foreach ($blogs as $blog)
                <div class="col-sm-4">
                    <div class="photo-blog-item"
                         data-image="{{ asset('storage/blog-image/' . $blog->image_large) }}"
                         data-title="{{ $blog->title_jp }}"
                         data-date="{{ $blog->posted_date->format('Y/m/d') }}"
                         data-post="{{ nl2br($blog->post_jp) }}"
                    >
                        <figure><img src="{{ asset('storage/blog-image/' . $blog->image_thumb) }}" alt="{{ $blog->title_jp }}"></figure>
                        <div class="photo-blog-item-info">
                            <h5>{{ $blog->title_jp }}</h5>
                            <span class="photo-blog-post-date">{{ $blog->posted_date->format('Y/m/d') }}</span>
                            <p>{{ $blog->excerpt_jp }}</p>
                            <a href="#">{{ __('Read More') }}</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- /Photo blog section -->

        <!-- Photo blog detail -->
        <div id="photo-blog-detail">
            <span class="photo-blog-detail-close"><i class="fal fa-times"></i></span>
            <!-- Generate the content dynamically inside this div -->
            <div class="photo-blog-detail-content">
                <h4 id="blog-detail-title"></h4>
                <span id="blog-detail-date" class="photo-blog-post-date"></span>
                <figure><img id="blog-detail-img" src="" alt=""></figure>
                <p id="blog-detail-post"></p>
            </div>
            <!-- /Generate dynamically -->
        </div>
        <!-- /Photo blog detail -->
    </div>
@endsection
