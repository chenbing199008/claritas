@extends('layouts.app', ['page' => 'home'])

@section('content')
<div id="main-wrapper">

    <!-- Home banner -->
    <div class="home-banner">
        <div class="home-banner-init">
            <div class="home-banner-item">
                {{--<figure><img src="{{ asset('img/home_banner/banner1.jpg') }}" alt="banner"></figure>--}}
                <figure><img src="{{ asset('mini-img/home_banner/banner1.jpg') }}" alt="banner"></figure>
                {{--<figure><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/cc745670de5f38ec666a261e97f9d385.jpg" alt="banner"></figure>--}}
            </div>
            <div class="home-banner-item">
                <figure><img src="{{ asset('mini-img/home_banner/banner2.jpg') }}" alt="banner"></figure>
                {{--<figure><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/b0902534f0e3fa788d4346887d020039.jpg" alt="banner"></figure>--}}
            </div>
            <div class="home-banner-item">
                <figure><img src="{{ asset('mini-img/home_banner/banner3.jpg') }}" alt="banner"></figure>
                {{--<figure><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/493eaf37d4da919f87bb3e3cc7b1d150.jpg" alt="banner"></figure>--}}
            </div>
            <div class="home-banner-item">
                <figure><img src="{{ asset('mini-img/home_banner/banner4.jpg') }}" alt="banner"></figure>
                {{--<figure><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/3c460950153e646267d35521cfb54b61.jpg" alt="banner"></figure>--}}
            </div>
        </div>
        <div class="container">
            <div class="home-banner-content">
                <h2>{{ __('home.hero-text') }}</h2>
                <figure><img src="{{ asset('mini-img/logo_banner.png') }}" alt="Logo"></figure>
                {{--<figure><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/feb83dc989d7aef434342c3483e3329b.png" alt="Logo"></figure>--}}
            </div>
        </div>
        <a href="#scroll-to-section" class="scroll-section">
            <span class="arrow-down"></span>
        </a>
    </div>
    <!-- /Home banner -->

    <div id="scroll-to-section">
        @include('frontend.menu')

        <!-- Home info section -->
        <div class="home-info-section">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-6 home-info-content-wrap">
                        <div class="home-info-content">
                            <h3>{{ __('home.about1.title') }}</h3>
                            <p>{!! __('home.about1.description') !!}</p>
                            <div class='d-none d-md-block'>
                                <p>&nbsp</p>
                                <p>&nbsp</p>
                                <p>&nbsp</p>
                                <p>&nbsp</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 home-info-image-wrap">
                        <figure class="home-slant-img"><img src="{{ asset('mini-img/services/service1.jpg') }}" alt="Providing global consultation for your Events and Projects"></figure>
                        {{--<figure class="home-slant-img"><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/5758e1e3484e26f849486ca5f55c17f0.jpg" alt="Providing global consultation for your Events and Projects"></figure>--}}
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-6 home-info-image-wrap">
                        <figure class="home-slant-img"><img src="{{ asset('mini-img/services/service2.jpg') }}" alt="We are event experts"></figure>
                        {{--<figure class="home-slant-img"><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/6a695bdb813b35d1b1df9372661d3889.jpg" alt="We are event experts"></figure>--}}
                    </div>
                    <div class="col-md-6 home-info-content-wrap">
                        <div class="home-info-content">
                            <h3>{{ __('home.about2.title') }}</h3>
                            <p>{!! __('home.about2.description') !!}</p>
                            <div class='d-none d-md-block'>
                                <p>&nbsp</p>
                                <p>&nbsp</p>
                                <p>&nbsp</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-6 home-info-content-wrap">
                        <div class="home-info-content">
                            <h3>{{ __('home.service.title') }}</h3>
                            <p>{!! __('home.service.description') !!}</p>
                            <ul>
                                <li>
                                    <h4>{{ __('home.service.service1.title') }}</h4>
                                    {!! __('home.service.service1.description') !!}
                                </li>
                                <li>
                                    <h4>{{ __('home.service.service2.title') }}</h4>
                                    {!! __('home.service.service2.description') !!}
                                </li>
                                <li>
                                    <h4>{{ __('home.service.service3.title') }}</h4>
                                    {!! __('home.service.service3.description') !!}
                                </li>
                                <li>
                                    <h4>{{ __('home.service.service4.title') }}</h4>
                                    {!! __('home.service.service4.description') !!}
                                </li>
                                <li>
                                    <h4>{{ __('home.service.service5.title') }}</h4>
                                    {!! __('home.service.service5.description') !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 home-info-image-wrap">
                        <figure class="home-slant-img"><img src="{{ asset('mini-img/services/service3.jpg')}}" alt="Services provided by claritas"></figure>
                        {{--<figure class="home-slant-img"><img src="https://sdkcoimagedebug.onemt.co/100002001/gm/check_in/100000601/22591f065597eae3d617b0cca84aac32.jpg" alt="Services provided by claritas"></figure>--}}
                    </div>
                </div>

                @if($videoUrl)
                    <div class="row" style="background: #000;padding: 30px;">
                        <div class="col-md-6 offset-md-3 col-sm-12">

                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{$videoUrl}}" allowfullscreen></iframe>
                            </div>

                            {{--<iframe src="{{$videoUrl}}" width="100%" height="415" frameborder="0" allowfullscreen></iframe>--}}
                        </div>

                    </div>
                @endif

            </div>
        </div>
        <!-- /Home info section -->
        @include('frontend.footer')
    </div>
</div>
@endsection
