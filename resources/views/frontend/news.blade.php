@extends('layouts.app')
<link href="{{ asset('css/semantic/semantic.min.css') }}" rel="stylesheet">
<script src="https://cdn.bootcss.com/vue/2.6.10/vue.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<link href="{{ asset('css/news.css?v=4') }}" rel="stylesheet">
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

@section('content')
    <div id="main-wrapper" v-cloak>
        @include('frontend.menu')

        {{--search--}}
        <div class="container-fluid search">
            <div class="col-md-6 offset-md-3 col-xs-10 offset-xs-1">
                <div class="ui search">
                    <div class="ui icon input search-input">
                        <input class="prompt" type="text" id="search-input" placeholder="Keyword"
                               v-model="query.keyWord" @keyup.enter="getPostsByKeyWord()">
                        <i class="search link icon search-icon" @click="getPostsByKeyWord()"></i>
                    </div>
                    <div class="results"></div>
                </div>
            </div>
        </div>

        <div class="container-fluid main-content">

            <div class="row">
                {{--time-line--}}
                <div class="col-2 time-line-wrap">
                    <div class="col-md-9 offset-md-3">
                        <ul class="time-line" v-for="timeLine, key in timeLines" :key="key">
                            <li class="year" v-html="timeLine.year"></li>
                            <li>
                                <div class="ui divider"></div>
                            </li>
                            <li v-for="month in timeLine.months" v-html="month.month"
                                :class="[query.date === month.link ? 'activeSelected' :'unactiveSelected']" @click="searchByMonth(month.link)"></li>
                        </ul>
                    </div>
                </div>

                <div class="col-0"></div>

                {{--posts--}}
                <div class="col-8 post-list">
                    <div>
                        <div v-if="query.page === 1 && showLatest">
                            <button class="mini ui yellow button" id="latest-button" v-if="latestPost.title">Latest post</button>
                            <h3 class="latest-post-title">@{{ latestPost.title}}</h3>
                            <p class="latest-post-created-time">@{{ latestPost.created }}</p>

                            <p class="latest-post-summary" v-html="latestPost.summary" v-if="!showLatestPostBody"></p>

                            <p class="latest-post-summary" v-html="latestPost.summary" v-if="showLatestPostBody"></p>
                            <p class="latest-post-body" v-html="latestPost.body" v-if="showLatestPostBody"></p>

                            <div class="latest-post-read-more" @click="readLatestMode()" v-if="!showLatestPostBody && latestPost.title">>> Read More</div>

                            <div v-if="showLatestPostBody">
                                <a class="latest-post-link" :href="latestPost.link" v-html="latestPost.link_name" target="_blank"></a>
                                <br>
                                <el-tag class="tag" type="info" v-for="tag, key in latestPost.tags" :key="key" v-html="tag"></el-tag>
                                <div class="latest-post-show-less" @click="lessLatestMode()">Show less</div>
                            </div>
                            <div class="ui divider"></div>
                        </div>


                        {{--post list--}}
                        <div v-loading="loading">
                            <div v-for="post, index in posts" :key="index">
                                <div class="row">
                                    <div class="col-1">
                                        <p class="created-time">@{{ post.created_at }}</p>
                                    </div>

                                    <div class="col-1"></div>

                                    <div class="col-10 post-wrap">
                                        <h3 class="title">@{{ post.title }}</h3>

                                        <p class="cut-summary" v-html="post.summary" v-if="!post.showAll"></p>

                                        <p class="summary" v-html="post.summary" v-if="post.showAll"></p>

                                        <p class="post-body" v-html="post.body" v-if="post.showAll"></p>

                                        <div class="read-more" @click="readMore(post)" v-if="!post.showAll">>> Read More</div>

                                        <div v-if="post.showAll">
                                            <a class="post-link" :href="post.link" v-html="post.link_name" target="_blank"></a>
                                            <br>
                                            <el-tag class="tag" type="info" v-for="tag, key in post.tags" :key="key" v-html="tag"></el-tag>
                                            <div class="show-less" @click="lessMore(post)">Show less</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ui divider"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row" v-if="this.posts.length > 0">
                        <div class="col-12 pagination">
                            <el-pagination
                                    size="medium"
                                    @current-change="pageChange"
                                    prev-text="< 前ページ"
                                    next-text="次ページ >"
                                    layout="prev, pager, next"
                                    :total="total">
                            </el-pagination>
                        </div>
                    </div>

                </div>
            </div>


        </div>

        @include('frontend.footer')
    </div>
    <script src="{{ asset('js/news.js') }}"></script>
@endsection
