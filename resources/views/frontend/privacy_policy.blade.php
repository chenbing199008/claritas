@extends('layouts.app')
@section('content')
    <div id="main-wrapper">
        @include('frontend.menu')
        <div class="policy-container">
            <section class="policy-section">
                <div class="title">個人情報保護方針</div>
                <div class="content">
                    <div class="part">
                        <div>株式会社クラリタスマーケティング（以下「当社」といいます）は、イベント・展示会運営の代行事業を展開しております。当社は、これらの業務の中で取得したお客様及び当社に関わるすべての個人情報に安全対策を施し、適切に保護する事が当社の責務と考えております。これらの事業は、関係者（お客様、お取引先様、及び従業員）の信頼の上に成り立っております。</div>
                        <div>当社は、事業活動を遂行するために、お預かりするすべての個人情報をより厳正に取り扱うため、役員及び従業員が遵守すべき行動基準として、本個人情報保護方針を定め、その遵守の徹底をはかります。</div>
                    </div>

                    <div class="part">
                        <ul>
                            <li>
                                <div class="li-title">
                                    マネジメントシステムの構築および継続的改善
                                </div>
                                <div class="part">
                                    <div>当社は、「個人情報保護マネジメントシステム－要求事項」（JISQ15001:2017）に準拠したマネジメントシステムを構築し、適切に運用いたします。</div>
                                    <div>また、構築したマネジメントシステムは、内部監査や代表者による見直し等の機会を通じて、継続的に改善してまいります。</div>
                                </div>
                            </li>

                            <li>
                                <div class="li-title">
                                    法令・規範の遵守
                                </div>
                                <div class="part">
                                    当社は、個人情報を取り扱うにあたり、個人情報に関する法令及び国が定める指針その他の規範、ガイドライン等を遵守いたします。
                                </div>
                            </li>

                            <li>
                                <div class="li-title">
                                    個人情報の取り扱い
                                </div>
                                <div class="part">
                                    当社事業、並びに従業員の雇用、人事管理等において取扱う個人情報について、予め特定された利用目的の範囲内において、個人情報の適切な取得・利用および提供を行い、利用目的の達成に必要な範囲を超えた個人情報の取扱い(目的外利用)は行いません。また、そのための適切な措置を講じます。
                                </div>
                            </li>

                            <li>
                                <div class="li-title">
                                    安全対策の実施
                                </div>
                                <div class="part">
                                    当社は、個人情報の正確性及び安全性を確保し、不正アクセスおよび、漏洩、紛失、破壊、改ざんなどを防止するため、個人情報へのアクセス管理、持ち出し手段の制限、情報セキュリティ対策等の安全対策を実施いたします。これらの安全対策は必要に応じ、見直し、改善してまいります。
                                    また、万一何らかの事故が発生した場合には、直ちに対策を講じ、再発防止に努めます。
                                </div>
                            </li>

                            <li>
                                <div class="li-title">
                                    苦情、相談、お問い合わせ
                                </div>
                                <div class="part">
                                    本方針及び、当社の取り扱う個人情報に関する苦情、相談、お問い合わせは、下記担当窓口までご連絡ください。速やかに対応いたします。
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        株式会社クラリタスマーケティング
                        <br>
                        代表取締役　 蔵田 昌子
                        <br>
                        2021年12月1日制定
                    </div>

                    <div class="part">
                        <div class="part-title">
                            個人情報 問い合わせ窓口
                        </div>
                        株式会社クラリタスマーケティング
                        <br>
                        個人情報保護管理者　柳崎　純
                        <br>
                        TEL　03-3710-8755
                    </div>
                </div>
            </section>

            <section class="policy-section">
                <div class="title">個人情報の取り扱いについて</div>
                <div class="content">
                    <div class="part">
                        <div class="part-title">個人情報の利用目的</div>
                        <div>
                            当社は個人情報について、利用目的を特定するとともに、法で定める場合等を除き、その利用目的の達成に必要な範囲内において利用いたします。
                        </div>
                    </div>

                    <div class="part">
                        <div class="part-title">取引先の個人情報</div>
                        <ul>
                            <li>
                                <div>
                                    業務上の連絡のため
                                </div>
                            </li>

                            <li>
                                <div>
                                    契約書や請求書作成及び代金振込のため
                                </div>
                            </li>

                            <li>
                                <div>
                                    取引履歴の管理のため
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        <div class="part-title">採用応募者の個人情報</div>
                        <ul>
                            <li>
                                <div>
                                    求人応募に対しての問い合わせ対応のため
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        <div class="part-title">従業員の個人情報</div>
                        <ul>
                            <li>
                                <div>
                                    社会保険関係書類作成のため
                                </div>
                            </li>
                            <li>
                                <div>
                                    給与支払いのため
                                </div>
                            </li>
                            <li>
                                <div>
                                    勤怠管理のため
                                </div>
                            </li>
                            <li>
                                <div>
                                    業務に関する連絡等のため
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        <div class="part-title">展示会及びセミナー参加者の個人情報</div>
                        <ul>
                            <li>
                                <div>
                                    展示会及びセミナー運営に関するお問い合わせに対する回答のため
                                </div>
                            </li>
                            <li>
                                <div>
                                    展示会及びセミナー開催リマインドメール送信のため
                                </div>
                            </li>
                            <li>
                                <div>
                                    展示会及びセミナーに関するアンケート取得のため
                                </div>
                            </li>
                            <li>
                                <div>
                                    商談会に関するスケジュール調整のため
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <section class="policy-section">
                <div class="title">共同利用について</div>
                <div class="content">
                    <div class="part">
                        当社が取得または保有する個人情報は以下の目的で共同利用いたします。
                    </div>

                    <div class="part">
                        <div class="part-title">共同して利用される個人情報の項目</div>
                        <div>
                            取引先担当者及び展示会やセミナー参加者の氏名、住所、電話番号、メールアドレス等
                        </div>
                    </div>

                    <div class="part">
                        <div class="part-title">共同して利用する者の範囲</div>
                        <div>
                            当社グループ企業であるClaritas Marketing Ltd.
                        </div>
                    </div>

                    <div class="part">
                        <div class="part-title">共同して利用する者の利用目的</div>
                        <ul>
                            <li>
                                <div>
                                    展示会及びセミナー運営に関するお問い合わせに対する回答のため
                                </div>
                            </li>

                            <li>
                                <div>
                                    展示会及びセミナー開催リマインドメール送信のため
                                </div>
                            </li>

                            <li>
                                <div>
                                    展示会及びセミナーに関するアンケート取得のため
                                </div>
                            </li>

                            <li>
                                <div>
                                    商談会に関するスケジュール調整のため
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        <div class="part-title">共同して利用する個人情報の管理について責任を有する者の氏名又は名称</div>
                        <div>
                            株式会社クラリタスマーケティング　個人情報保護管理者　柳崎　純
                        </div>
                    </div>

                    <div class="part">
                        <div class="part-title">取得方法</div>
                        <div>
                            電子データ、紙媒体、Web等より取得
                        </div>
                    </div>
                </div>
            </section>

            <section class="policy-section">
                <div class="title">苦情、相談、お問い合わせ</div>
                <div class="content">
                    <div class="part">
                        当社の取り扱う個人情報に関する苦情、相談、お問い合わせは、下記担当窓口までご連絡ください。
                        速やかに対応いたします。
                    </div>

                    <div class="part">
                        <div class="part-title">苦情・相談・お問い合わせ窓口</div>
                        <div>
                            <div>株式会社クラリタスマーケティング</div>
                            <div>個人情報保護管理者 柳崎　純</div>
                            <div>TEL　03-3710-8755</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="policy-section">
                <div class="title">個人情報の開示等請求の手続き</div>
                <div class="content">
                    <div class="part">
                        当社は、当社が個人情報を取得したご本人、またはその代理人から、利用目的の通知、開示、内容の訂正、追加または削除、利用の停止、消去及び第三者への提供の停止（「開示等」といいます。）を求められた場合、所定の手続きにより、速やかに対応いたします。
                    </div>

                    <div class="part">
                        <div class="part-title">開示等請求手続きの流れ</div>
                        <ul>
                            <li>
                                <div>
                                    開示等ご請求窓口へ、お電話で申し込み
                                    <br>
                                    <div class="tab-down">↓</div>
                                </div>
                            </li>

                            <li>
                                <div>
                                    当社より所定の書類を郵送
                                    <br>
                                    <div class="tab-down">↓</div>
                                </div>
                            </li>

                            <li>
                                <div>
                                    必要書類と手数料（利用目的の通知・開示の場合）を開示等ご請求窓口へ郵送
                                    <br>
                                    <div class="tab-down">↓</div>
                                </div>
                            </li>

                            <li>
                                <div>
                                    当社にて必要書類及び手数料を確認し、開示等請求に対する回答を郵送
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        <div class="part-title">開示等の請求先</div>
                        <div>
                            開示等のご請求は、開示等ご請求窓口まで、お電話でお申し込みください。
                            当社所定の請求書を郵送いたします。
                        </div>
                    </div>

                    <div class="part">
                        <div class="part-title">開示等ご請求窓口</div>
                        <div>
                            <div>株式会社クラリタスマーケティング</div>
                            <div>個人情報保護管理者 柳崎　純</div>
                            <div>TEL　03-3710-8755</div>
                        </div>
                    </div>

                    <div class="part">
                        <div class="part-title">開示等の請求に必要な書類</div>
                        <ul>
                            <li>
                                <div class="li-title">
                                    ご本人によるご請求の場合
                                </div>
                                <div>
                                    <ul class="circle-number-ul">
                                        <li>
                                            ① 当社所定の請求書
                                        </li>
                                        <li>
                                            ② 本人確認のための書類（a～dいずれかのコピー1通）
                                            <ul class="lower-alpha-ul">
                                                <li>運転免許証（表・裏ともに）※注</li>
                                                <li>パスポート</li>
                                                <li>c.健康保険証</li>
                                                <li>年金手帳</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <div class="li-title">
                                    代理人によるご請求の場合（上記①、②に加え、③、④が必要となります）
                                </div>
                                <div>
                                    <ul class="circle-number-ul">
                                        <li class="mb-3">
                                            ③ 代理権確認のための書類（a、ｂいずれか）
                                            <ul class="list-style-none">
                                                <li>
                                                    法定代理人（親権者または成年後見人）の場合
                                                    <ul class="lower-alpha-ul">
                                                        <li>
                                                            戸籍謄本、または成年後見登記事項証明書等、法定代理権があることを確認できる書類
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    任意代理人の場合
                                                    <ul class="list-style-none">
                                                        <li>
                                                            b. 委任状（本人の署名捺印のあるもの）及び印鑑登録証明書（委任状に捺印された本人の印鑑のもの）
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            ④ 代理人確認のための書類（a～dいずれかのコピー1通）
                                            <ul class="lower-alpha-ul mb-3">
                                                <li>
                                                    運転免許証（表・裏ともに）※注
                                                </li>
                                                <li>
                                                    パスポート
                                                </li>
                                                <li>
                                                    健康保険証
                                                </li>
                                                <li>
                                                    年金手帳
                                                </li>
                                            </ul>
                                            ※注　運転免許証のコピーをご提示頂く場合は、本籍地をマジックで塗りつぶして下さい。
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="mt-4">
                                <div class="li-title">
                                    開示等の請求に必要な手数料およびその徴収方法
                                </div>
                                <div>
                                    <div>利用目的の通知、または開示をご請求の場合、手数料を頂戴しております。</div>
                                    <div>必要書類をご郵送いただく際、以下の金額の郵便切手を同封してください。</div>
                                    <div class="font-weight-bolder pl-5 pt-2 pb-2">
                                        <span class="mr-4">
                                            利用目的の通知、開示のご請求一件につき
                                        </span>
                                        <span>
                                            １０００円
                                        </span>
                                    </div>
                                    <div>その他のご請求（内容の訂正、追加または削除、利用の停止、消去及び第三者への提供の停止）の場合、手数料は不要です。</div>
                                </div>
                            </li>

                            <li class="mt-4">
                                <div class="li-title">
                                    開示等の請求に対する回答
                                </div>
                                <div>
                                    ご本人（代理人請求の場合は代理人）の請求書記載住所宛に、回答を郵送いたします。
                                </div>
                            </li>

                            <li class="mt-4">
                                <div class="li-title">
                                    開示等の請求に応じられない場合
                                </div>
                                <div>
                                    <div>以下の①～⑥に該当する場合、開示等のご請求にお応えできません。お応えできない場合は、</div>
                                    <div class="mb-3">その旨理由を付記して通知いたします。手数料の返却は致しかねますので、ご了承ください。</div>
                                    <ul class="circle-number-ul">
                                        <li>① 本人確認ができない場合</li>
                                        <li>② 代理人による申請に際して、代理権が確認できない場合</li>
                                        <li>③ 所定の請求書類に不備があった場合</li>
                                        <li>④ 本人または第三者の生命、身体、財産その他の権利利益を害するおそれがある場合</li>
                                        <li>⑤ 当社の業務の適正な実施に著しい支障を及ぼすおそれがある場合</li>
                                        <li>⑥ 法令に違反することとなる場合</li>
                                    </ul>
                                </div>
                            </li>

                            <li class="mt-4">
                                <div class="li-title">
                                    開示等の請求で取得した個人情報の利用目的
                                </div>
                                <div>
                                    開示等のご請求にともなって取得した個人情報は、開示等のご請求に必要な範囲でのみ取り扱うものとします。
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="part">
                        <div class="part-title">以上</div>
                    </div>


                </div>
            </section>

        </div>

        @include('frontend.footer')
    </div>
@endsection
