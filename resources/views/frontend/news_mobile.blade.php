@extends('layouts.app')
<link href="{{ asset('css/semantic/semantic.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/ele.css') }}" rel="stylesheet">
<link href="{{ asset('css/news-mobile.css?v=3') }}" rel="stylesheet">
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/ele.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>

@section('content')
    <div id="main-wrapper" v-cloak>
        @include('frontend.menu')

        {{--search--}}
        <div class="row search">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="ui search">
                    <div class="ui icon input search-input">
                        <input class="prompt" type="text" id="search-input" placeholder="Keyword"
                               v-model="query.keyWord" @keyup.enter="getPostsByKeyWord()">
                        <i class="search link icon search-icon" @click="getPostsByKeyWord()"></i>
                    </div>
                </div>
            </div>
        </div>

        {{--date picker--}}
        <div class="row date-picker">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="form-group">
                    <select class="form-control" @change="changeDate()" v-model="selectedDate">
                        <option value="">ALL</option>
                        <option :value="timeLine.link" v-for="timeLine in timeLines">@{{timeLine.label}}
                        </option>
                    </select>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="row post-body-all">
                {{--posts--}}
                <div class="col-12" v-if="query.page === 1 && showLatest">


                    <button class="mini ui yellow button" id="latest-button" v-if="latestPost.title">Latest post</button>
                    <h5 class="latest-post-title" v-if="latestPost.title">@{{ latestPost.title }}</h5>
                    <h6 class="latest-post-created-time" v-if="latestPost.title">@{{ latestPost.created }}</h6>
                    <h6 class="latest-post-summary" v-html="latestPost.summary" v-if="!showLatestPostBody"></h6>
                    <h6 class="latest-post-summary" v-html="latestPost.summary" v-if="showLatestPostBody"></h6>
                    <h6 class="latest-post-body" v-html="latestPost.body"  v-if="showLatestPostBody"></h6>

                    <div class="read-more" @click="readLatestMode()" v-if="!showLatestPostBody && latestPost.title">
                        >> Read More
                    </div>

                    <div v-if="showLatestPostBody">
                        <a class="latest-post-link" :href="latestPost.link" v-html="latestPost.link_name" target="_blank"></a>
                        <div class="row tag-list">
                            <el-tag class="tag" type="info" v-for="tag, key in latestPost.tags" :key="key" v-html="tag"></el-tag>
                        </div>

                        <div class="show-less" @click="lessLatestMode()">Show less</div>
                    </div>

                    <div class="ui divider" v-if="query.page === 1 && showLatest"></div>
                </div>


                {{--post list--}}
                <div v-loading="loading">
                    <div v-for="post, index in posts" :key="index">
                        <div class="col-12">
                            <h6 class="created-time">@{{ post.created_at }}</h6>
                        </div>

                        <div class="col-12">
                            <h6 class="title" v-html="post.cutTitle" v-if="!post.showAll"></h6>

                            <h6 class="title" v-if="post.showAll" v-html="post.title"></h6>

                            <h6 class="summary" v-if="post.showAll" v-html="post.summary"></h6>

                            <h6 class="post-body" v-html="post.body" v-if="post.showAll"></h6>

                            <div class="read-more" @click="readMore(post)" v-if="!post.showAll">>> Read More</div>

                            <div v-if="post.showAll">
                                <a class="post-link" :href="post.link" v-html="post.link_name" target="_blank"></a>

                                <div class="row tag-list">
                                    <el-tag class="tag" type="info" v-for="tag, key in post.tags" :key="key" v-html="tag"></el-tag>
                                </div>

                                <div class="show-less" @click="lessMore(post)">Show less</div>
                            </div>
                        </div>

                        <div class="ui divider"></div>
                    </div>


                </div>
            </div>
        </div>


        <div class="row" v-if="this.posts.length > 0">
            <div class="col-12 pagination">
                <el-pagination
                        @current-change="pageChange"
                        prev-text="< 前ページ"
                        next-text="次ページ >"
                        :page-size="20"
                        layout="prev, pager, next"
                        :total="total">
                </el-pagination>
            </div>
        </div>


        @include('frontend.footer')
    </div>
    <script src="{{ asset('js/news-mobile.js') }}"></script>


@endsection
