@extends('layouts.app')
@section('content')
    <div id="main-wrapper">
    @include('frontend.menu')

    <!-- Gallery Info -->
        <div class="gallery-info clearfix">
            <div class="container">
            @foreach($galleries as $gallery)
                <!-- Standard Construction -->
                    <div class="gallery-info-item clearfix">
                        <h3 class="text-center">{{ $gallery->{'name_' . LANG_FIELD} }}</h3>
                        <div class="gallery-info-big-slider-wrap">
                            <div class="gallery-info-big-slider-init">
                                @foreach ($gallery->primaryImages as $image)
                                    <div class="gallery-info-magnific">
                                        <a class="gallery-info-vertical-fit gallery-info-stand-construction"
                                           href="{{ '/storage/gallery-primary-image/' . $image->image_large }}"
                                           data-caption='{{ $image->title }} <br /> {!! nl2br($image->inside_text ?? $image->outside_text) !!}'>
                                            <figure><img
                                                        src="{{ '/storage/gallery-primary-image/' . $image->image_large }}"
                                                        alt="{{ $image->title }}"></figure>
                                        </a>
                                        <h4 class="gallery-info-caption-normal">{{ $image->title }}
                                            <br/> {!! nl2br($image->outside_text ?? $image->inside_text) !!}</h4>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="gallery-thumb-slider" data-fluffy-container>
                            <div class="gallery-thumb-content" data-fluffy-content>
                                @foreach ($gallery->primaryImages as $image)
                                    <div class="gallery-thumb-item">
                                        <a
                                                class="gallery-thumbnail gallery-info-stand-construction-thumbnail"
                                                data-caption='{{ $image->title }} <br /> {!! nl2br($image->inside_text ?? $image->outside_text) !!}'
                                                href="{{ '/storage/gallery-primary-image/' . $image->image_large }}"
                                        >
                                            <img src="{{ '/storage/gallery-primary-image/' . $image->image_thumb }}">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /Standard Construction -->
                @endforeach
            </div>
        </div>
        <!-- /Gallery Info -->
        @include('frontend.footer')
    </div>
@endsection
