@extends('layouts.app')


<script src="{{ asset('js/jquery.min.js') }}?v=1.7"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
<script src="https://cdn.bootcss.com/vue/2.6.10/vue.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>




@section('content')
    <div id="main-wrapper">
    @include('frontend.menu')

    <!-- Contact Info -->
        <div class="contact-info" id="contact-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>{{ __("London") }}</h2>
                        <ul class="contact-info-list">
                            <li><h5>Claritas Marketing Ltd.</h5></li>
                            <li>6 Aztec Row, Berners Road, London N1 0PW</li>
                            <li>United Kingdom <br/><br/></li>
                            <li><a href="tel:+44 (0) 20 7794 1123">+44 (0) 20 7794 1123</a> <br/></li>
                        </ul>
                        <div id="map-london"></div>
                    </div>
                    <div class="col-md-6">
                        <h2>{{ __("Tokyo") }}</h2>
                        <ul class="contact-info-list">
                            <li><h5>{{__('contact.right.business_name')}}</h5></li>
                            <li>{{__('contact.right.address_1')}}</li>
                            <li>{{__('contact.right.address_2')}}<br/><br/></li>
                            <li>TEL: <a href="+81 (0)3 3710 8755">+81 (0)3 3710 8755</a> / FAX: +81 (0)3 6451 0812 <br/>
                            </li>
                        </ul>
                        <div id="map-japan"></div>
                    </div>
                </div>

                <div>
                    <h2 class="mb-5 mt-5">{{ __("work.contact_us") }}</h2>
                    <div>
                        <form class="ui form contact-form">
                            <div class="inline fields">
                                <label class="mb-2 choose-office-label">{{ __('work.choose_office') }} : </label>
                                @if ($language === 'ja')
                                    <div class="field">
                                        <div class="ui checkbox">
                                            <input type="radio" name="frequency" checked="checked" v-model="form.office" value="1">
                                            <label>{{__('work.tokyo_office') }}</label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui checkbox">
                                            <input type="radio" name="frequency" v-model="form.office" value="2">
                                            <label>{{__('work.london_office') }}</label>
                                        </div>
                                    </div>
                                @else
                                    <div class="field">
                                        <div class="ui checkbox">
                                            <input type="radio" name="frequency" checked="checked"  v-model="form.office" value="2">
                                            <label>{{__('work.london_office') }}</label>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <div class="ui checkbox">
                                            <input type="radio" name="frequency" v-model="form.office" value="1">
                                            <label>{{__('work.tokyo_office') }}</label>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="field" :class="[errors.name ? 'error' : '']">
                                <input type="text" placeholder="{{__('contact.placeholder.name')}}" v-model="form.name">
                            </div>
                            <div class="field" :class="[errors.companyName ? 'error' : '']">
                                <input type="text" placeholder="{{__('contact.placeholder.companyName')}}" v-model="form.companyName">
                            </div>
                            <div class="field" :class="[errors.tel ? 'error' : '']">
                                <input type="text" placeholder="{{__('contact.placeholder.tel')}}" v-model="form.tel">
                            </div>
                            <div class="field" :class="[errors.email ? 'error' : '']">
                                <input type="email" placeholder="{{__('contact.placeholder.email')}}" v-model="form.email">
                            </div>
                            <div class="field" :class="[errors.message ? 'error' : '']">
                                <textarea placeholder="{{__('contact.placeholder.message')}}" v-model="form.message"></textarea>
                            </div>

                            @if(app()->getLocale() === "ja")
                                <div class="field">
                                    <div>個人情報保護方針に同意の上、お問い合わせ内容を送信してください。</div>
                                    <div class="mb-1">※最後までお読みください。</div>
                                    <div class="read-text" style="height: 200px" @scroll="handleScroll">
                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報の取り扱いについて
                                            </div>
                                            <p>お問い合わせいただく内容は、個人情報を含む場合があります。下記をご覧いただき、同意の上、お問い合わせ下さい。</p>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報の利用目的
                                            </div>
                                            <p>当社が取得したすべての個人情報は、お問い合わせに対する連絡を目的として利用します。</p>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報の提供・取扱いの委託
                                            </div>
                                            <p>取得した個人情報を第三者に提供・取り扱いの委託をすることはありません。</p>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                共同利用について
                                            </div>
                                            <p>当社が取得または保有する個人情報は以下の目的で共同利用いたします。</p>
                                        </div>

                                        <div class="mb-3">
                                            <div>
                                                ①共同して利用される個人情報の項目
                                            </div>
                                            <p class="ml-3">取引先担当者及び展示会やセミナー参加者の氏名、住所、電話番号、メールアドレス等</p>
                                        </div>

                                        <div class="mb-3">
                                            <div>
                                                ②共同して利用する者の範囲
                                            </div>
                                            <p class="ml-3">当社グループ企業である Claritas Marketing Ltd.</p>
                                        </div>

                                        <div class="mb-3">
                                            <div>
                                                ③共同して利用する者の利用目的
                                            </div>
                                            <div class="ml-3">展示会及びセミナー運営に関するお問い合わせに対する回答のため</div>
                                            <div class="ml-3">展示会及びセミナー開催リマインドメール送信のため</div>
                                            <div class="ml-3">商談会に関するスケジュール調整のため</div>
                                        </div>

                                        <div class="mb-3">
                                            <div>
                                                ④共同して利用する個人情報の管理について責任を有する者の氏名又は名称
                                            </div>
                                            <p class="ml-3">株式会社クラリタスマーケティング　個人情報保護管理者　柳崎　純</p>
                                        </div>

                                        <div class="mb-3">
                                            <div>
                                                ⑤取得方法
                                            </div>
                                            <p class="ml-3">電子データ、紙媒体、Web 等より取得</p>
                                        </div>


                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報の問い合わせ窓口
                                            </div>
                                            <div>株式会社クラリタスマーケティング</div>
                                            <div>個人情報保護管理者 柳崎　純</div>
                                            <div>TEL　03-3710-8755</div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報を入力するにあたっての注意事項
                                            </div>
                                            <div>必須項目のご入力がない場合は、ご連絡出来ないことがありますので、予めご了承ください。</div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                本人が容易に認識できない方法による個人情報の取得
                                            </div>
                                            <div>クッキー (cookie)、ウェブビーコン (web beacon) 、広告用識別子などの技術を使用して個人情報を取得する場合があります。</div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報の安全管理措置について
                                            </div>
                                            <div>取得した個人情報については、漏えい、滅失またはき損の防止と是正、その他個人情報の安全管理のために必要かつ適切な措置を講じます。</div>
                                            <div>このサイトは、SSL(Secure Socket Layer) による暗号化措置を講じています。</div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                個人情報の開示等請求の手続き
                                            </div>
                                            <div>当社は当社が個人情報を取得したご本人、またはその代理人から、利用目的の通知、開示、内容の訂正、追加又は削除、利用の停止、消去及び第三者への提供の停止（「開示等」といいます。）を求められた場合、 所定の手続きにより、速やかに対応いたします。</div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                開示等の請求先
                                            </div>
                                            <div>開示等のご請求は、開示等ご請求窓口まで、お電話でお申し込みください。当社所定の請求書を郵送いたします。</div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="font-weight-bolder">
                                                開示等ご請求窓口
                                            </div>
                                            <div>株式会社クラリタスマーケティング</div>
                                            <div>個人情報保護管理者　柳崎　純</div>
                                            <div>TEL　03-3710-8755</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="field text-center mt-5">
                                    <div class="ui checkbox position-relative" @click="showUnReadAllNotice()">
                                        <el-tooltip class="item" effect="dark" placement="top-start" v-model="showUnReadNotice" effect="light">
                                            <div slot="content"><i class="el-icon-warning mr-3"></i>個人情報保護方針に同意の上、送信してください</div>
                                            <span style="margin-left: 5px; left: 0;" class="position-absolute"></span>
                                        </el-tooltip>
                                        <input type="checkbox" name="frequency2" v-model="form.protection" v-if="hasReadAll">
                                        <input type="checkbox" name="frequency2" v-model="form.protection" disabled v-else>
                                        <label class="font-weight-bolder">個人情報保護方針に同意する</label>
                                    </div>
                                </div>
                            @endif

                            <div class="field text-center mt-4" style="display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;">
                                <div class="errors text-left mb-5" v-if="errorMessages.length > 0 || unReadAllText">
                                    <span class="text-danger" v-if="unReadAllText">* 個人情報保護方針を最後までお読みください</span>
                                    <div v-for="errorMessage in errorMessages">
                                        <span class="text-danger" v-if="errorMessage == 1"> * {{ __('contact.errors.name') }}</span>
                                        <span class="text-danger" v-if="errorMessage == 2"> * {{ __('contact.errors.companyName') }}</span>
                                        <span class="text-danger" v-if="errorMessage == 3"> * {{ __('contact.errors.tel') }}</span>
                                        <span class="text-danger" v-if="errorMessage == 4"> * {{ __('contact.errors.email') }}</span>
                                        <span class="text-danger" v-if="errorMessage == 5"> * {{ __('contact.errors.message') }}</span>
                                    </div>
                                </div>
                                <button class="ui button contact-send-button" type="button" @click="send()">{{__('contact.send')}}</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>



        <!-- /Contact Info -->

        <!-- Footer section -->
        <footer class="main-footer">
            <div class="container">
                <p class="copyright-text">{{ __('footer.copyright') }}</p>
            </div>
        </footer>
        <!-- /Footer section -->
    </div>

    <script>

        var language = '{{ $language }}'



        function initMap() {
            var myLondonLatLng = {
                lat: {{ setting('OFFICE_LONDON_LATITUDE') }},
                lng: {{ setting('OFFICE_LONDON_LONGITUDE') }} };
            var myJapanLatLng = {
                lat: {{ setting('OFFICE_JAPAN_LATITUDE') }},
                lng: {{ setting('OFFICE_JAPAN_LONGITUDE') }} };

            // Create a map object and specify the DOM element
            // for display.
            var myLondonmap = new google.maps.Map(document.getElementById('map-london'), {
                center: myLondonLatLng,
                zoom: 17,
                styles: [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]
            });

            var myJapanmap = new google.maps.Map(document.getElementById('map-japan'), {
                center: myJapanLatLng,
                zoom: 16,
                styles: [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]
            });

            var pointerImage = ('{{ asset('img/pointer.png') }}');

            // Create a marker for London and set its position.
            var marker = new google.maps.Marker({
                map: myLondonmap,
                position: myLondonLatLng,
                icon: pointerImage,
                title: 'We are here!'
            });

            // Create a marker for Japan and set its position.
            var marker = new google.maps.Marker({
                map: myJapanmap,
                position: myJapanLatLng,
                icon: pointerImage,
                title: '私たちはここにいる!'
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ setting('GOOGLE_MAP_API_KEY') }}&callback=initMap" async defer></script>
    <script src="{{ asset('js/contact.js') }}"></script>
@endsection
