<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Claritas') }}</title>

    <!-- Styles -->
    <meta name="description" content="{{ __('home.meta') }}">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

{{--    <link rel="manifest" href="{{ asset('site.webmanifest') }}">--}}
    <link rel="shortcut icon" href="{{ asset('icon.jpg') }}">
    <!-- Place favicon.ico in the root directory -->
    {{--<link href="{{ asset('css/semantic/semantic.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/semantic/components/icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.min.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/OverlayScrollbars.min.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/fluffy.min.css') }}?v=1.7" rel="stylesheet">
    <link href="https://photoswipe.com/dist/photoswipe.css?v=4.1.3-1.0.4?v=1.7" rel="stylesheet">
    <link href="https://photoswipe.com/dist/default-skin/default-skin.css?v=4.1.3-1.0.4?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/main.min.css') }}?v=1.8" rel="stylesheet">


    <style>
        @media screen and (min-width: 1700px) {
            .main-navigation .navbar-toggler[aria-expanded=true] {
                -webkit-transform: scale(0.7) rotate(90deg);
                transform: scale(0.7) rotate(90deg);
            }
        }

        @media screen and (min-width: 992px) {
            .main-navigation .navbar-toggler[aria-expanded=true] {
                -webkit-transform: scale(0.7) rotate(90deg);
                transform: scale(0.7) rotate(90deg);
            }
        }

        @media screen and (min-width: 1720px) {
            .main-navigation .navbar-nav .nav-link {
                padding: 10px 15px;
            }
        }

        @media screen and (min-width: 992px) {
            .main-navigation .navbar-nav .nav-link {
                padding: 10px 15px;
            }
        }
        @media (max-width: 1719px) {
            .main-navigation .navbar .navbar-collapse {
                border: 0;
                position: absolute;
            }

        }

        @media (min-width: 1720px) {
            .navbar-expand-cb {
                flex-flow: row nowrap;
                justify-content: flex-start;
            }

            .navbar-expand-cb .navbar-nav {
                flex-direction: row;
            }

            .navbar-expand-cb .navbar-nav .dropdown-menu {
                position: absolute;
            }

            .navbar-expand-cb .navbar-nav .nav-link {
                padding-right: 0.5rem;
                padding-left: 0.5rem;
            }

            .navbar-expand-cb > .container,
            .navbar-expand-cb > .container-fluid {
                flex-wrap: nowrap;
            }

            .navbar-expand-cb .navbar-collapse {
                display: flex !important;
                flex-basis: auto;
            }

            .navbar-expand-cb .navbar-toggler {
                display: none;
            }

            .main-navigation .navbar .navbar-collapse {
                border: 0;
                position: static;
            }
        }


    </style>
    @yield('head')
</head>

<body id="{{ $page ?? '' }}">
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->
@yield('content')
<script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}?v=1.7"></script>
<script src="{{ asset('js/jquery.min.js') }}?v=1.7"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/vendor/jquery-3.3.1.min.js') }}?v=1.7"><\/script>')</script>
<script src="{{ asset('js/vendor/jquery-3.3.1.min.js') }}?v=1.7"></script>
<script src="{{ asset('js/jquery.overlayScrollbars.min.js') }}?v=1.7"></script>
<script src="{{ asset('js/slick.min.js') }}?v=1.7"></script>
<script src="{{ asset('js/fluffy.min.js') }}?v=1.7"></script>
<script src="https://photoswipe.com/dist/photoswipe.min.js?v=4.1.3-1.0.4"></script>
<script src="https://photoswipe.com/dist/photoswipe-ui-default.min.js?v=4.1.3-1.0.4"></script>
<script src="{{ asset('js/jqPhotoSwipe.js') }}?v=1.7"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}?v=1.7"></script>
<script src="{{ asset('js/image-map-resizer.js') }}?v=1.7"></script>
<script src="{{ asset( app()->getLocale() . '/data.js') }}?v=1.7"></script>
<script src="{{ asset('js/plugins.js') }}?v=1.7"></script>
<script src="{{ asset('js/main.js') }}?v=1.7"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173959782-1"></script>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->

<script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', '{{ setting('GOOGLE_TRACKING_ID') }}', 'auto'); ga('send', 'pageview');
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-173959782-1');
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

<link href="{{ asset('css/app.css') }}?v=21" rel="stylesheet">


<!-- Global site tag (gtag.js) - Google Analytics -->



</body>

</html>
