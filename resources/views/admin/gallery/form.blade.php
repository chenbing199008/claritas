<div class="form-group{{ $errors->has('name_en') ? 'has-error' : ''}}">
    {!! Form::label('name_en', 'Name En', ['class' => 'control-label']) !!}
    {!! Form::text('name_en', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name_jp') ? 'has-error' : ''}}">
    {!! Form::label('name_jp', 'Name Jp', ['class' => 'control-label']) !!}
    {!! Form::text('name_jp', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_jp', '<p class="help-block">:message</p>') !!}
</div>
{{--<div class="form-group{{ $errors->has('outside_text') ? 'has-error' : ''}}">--}}
{{--    {!! Form::label('outside_text', 'Outside Text', ['class' => 'control-label']) !!}--}}
{{--    <small>The text that displays on the white screen</small>--}}
{{--    {!! Form::textarea('outside_text', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}--}}
{{--    {!! $errors->first('outside_text', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}
{{--<div class="form-group{{ $errors->has('inside_text') ? 'has-error' : ''}}">--}}
{{--    {!! Form::label('inside_text', 'Inside Text', ['class' => 'control-label']) !!}--}}
{{--    <small>The text that displays on the black screen on pop up</small>--}}
{{--    {!! Form::textarea('inside_text', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}--}}
{{--    {!! $errors->first('inside_text', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}
<div class="form-group{{ $errors->has('sort_order') ? 'has-error' : ''}}">
    {!! Form::label('sort_order', 'Sort Order', ['class' => 'control-label']) !!}
    {!! Form::number('sort_order', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
