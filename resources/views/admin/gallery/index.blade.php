@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Gallery</div>
                    <div class="card-body">
{{--                        <a href="{{ url('/admin/gallery/create') }}" class="btn btn-success btn-sm" title="Add New Gallery">--}}
{{--                            <i class="fa fa-plus" aria-hidden="true"></i> Add New--}}
{{--                        </a>--}}

{{--                        Outside Text--}}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name En</th><th>Sort Order</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($gallery as $item)
                                    <tr>
                                        <td>{{ $loop->iteration}}</td>
                                        <td>{{ $item->name_en }}</td><td>{{ $item->sort_order }}</td>
                                        <td>
                                            <a title='Big Images Slider' class='btn btn-dark btn-sm' href='{{ url('/admin/gallery/' . $item->id . '/primary')  }}'>
                                                Images
                                            </a>
{{--                                            <a title='Small Image Slider under' class='btn btn-dark btn-sm' href='{{ url('/admin/gallery/' . $item->id . '/secondary')  }}'>--}}
{{--                                                Secondary Images--}}
{{--                                            </a>--}}
{{--                                            <a href="{{ url('/admin/gallery/' . $item->id) }}" title="View Gallery"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>--}}
                                            <a href="{{ url('/admin/gallery/' . $item->id . '/edit') }}" title="Edit Gallery"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
{{--                                            {!! Form::open([--}}
{{--                                                'method' => 'DELETE',--}}
{{--                                                'url' => ['/admin/gallery', $item->id],--}}
{{--                                                'style' => 'display:inline'--}}
{{--                                            ]) !!}--}}
{{--                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(--}}
{{--                                                        'type' => 'submit',--}}
{{--                                                        'class' => 'btn btn-danger btn-sm',--}}
{{--                                                        'title' => 'Delete Gallery',--}}
{{--                                                        'onclick'=>'return confirm("Confirm delete?")'--}}
{{--                                                )) !!}--}}
{{--                                            {!! Form::close() !!}--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $gallery->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
