@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Blog {{ $blog->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/blog') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/blog/' . $blog->id . '/edit') }}" title="Edit Blog"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['blog', $blog->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Blog',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $blog->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Posted On</th>
                                        <td>{{ $blog->posted_date->format('m/d/Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th> Status </th><td> {{ $blog->status }} </td>
                                    </tr>
                                    <tr>
                                        <th> Title Jp </th><td> {{ $blog->title_jp }} </td>
                                    </tr>
                                    <tr>
                                        <th> Excerpt Jp </th><td> {{ $blog->excerpt_jp }} </td>
                                    </tr>
                                    <tr>
                                        <th> Post Jp </th><td> {{ $blog->post_jp }} </td>
                                    </tr>
                                    <tr>
                                        <th>Image Thumb</th>
                                        <td><img src='/storage/blog-image/{{ $blog->image_thumb }}' width='300'></td>
                                    </tr>
                                    <tr>
                                        <th>Image Large</th>
                                        <td><img src='/storage/blog-image/{{ $blog->image_large }}' width='500'></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
