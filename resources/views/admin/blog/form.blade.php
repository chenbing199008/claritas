<div class="form-group{{ $errors->has('title_jp') ? 'has-error' : ''}}">
    {!! Form::label('title_jp', 'Title Jp', ['class' => 'control-label']) !!}
    {!! Form::text('title_jp', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title_jp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image_large') ? 'has-error' : ''}}">
    {!! Form::label('image_large', 'Image Large', ['class' => 'control-label']) !!}
    <small>1020px x 350px</small>
    {!! Form::file('image_large', ($formMode == 'create') ? ['class' => 'form-control', 'required' => 'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('image_large', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image_thumb') ? 'has-error' : ''}}">
    {!! Form::label('image_thumb', 'Image Thumb', ['class' => 'control-label']) !!}
    <small>365px x 305px</small>
    {!! Form::file('image_thumb', ($formMode == 'create') ? ['class' => 'form-control', 'required' => 'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('image_thumb', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('excerpt_jp') ? 'has-error' : ''}}">
    {!! Form::label('excerpt_jp', 'Excerpt Jp', ['class' => 'control-label']) !!}
    {!! Form::textarea('excerpt_jp', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('excerpt_jp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('post_jp') ? 'has-error' : ''}}">
    {!! Form::label('post_jp', 'Post Jp', ['class' => 'control-label']) !!}
    {!! Form::textarea('post_jp', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('post_jp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('posted_date') ? 'has-error' : ''}}">
    {!! Form::label('posted_date', 'Posted Date', ['class' => 'control-label']) !!}
    {!! Form::input('date', 'posted_date', $now ?? $blog->posted_date->format('Y-m-d'), ['class' => 'form-control','required' => 'required']) !!}
    {!! $errors->first('posted_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('status', $statuses, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
