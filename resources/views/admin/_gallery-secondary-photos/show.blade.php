@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">GallerySecondaryPhoto {{ $gallerysecondaryphoto->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/gallery-secondary-photos') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/gallery-secondary-photos/' . $gallerysecondaryphoto->id . '/edit') }}" title="Edit GallerySecondaryPhoto"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/gallerysecondaryphotos', $gallerysecondaryphoto->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete GallerySecondaryPhoto',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $gallerysecondaryphoto->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $gallerysecondaryphoto->title }} </td></tr><tr><th> Image Thumb </th><td> {{ $gallerysecondaryphoto->image_thumb }} </td></tr><tr><th> Image Large </th><td> {{ $gallerysecondaryphoto->image_large }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
