@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        Project <i class='fa fa-angle-right'></i>
                        {{ $project->name_en }} <i class='fa fa-angle-right'></i>
                        Project Images</div>
                    <div class="card-body">
                        <a href="{{ route('project.index') }}"
                           title="Back"><button class="btn btn-warning btn-sm"><i class="fa
                        fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ route('project.image.create', ['project' => $project->id]) }}"
                           class="btn btn-success btn-sm"
                           title="Add New ProjectImage">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

{{--                        {!! Form::open([--}}
{{--                            'method' => 'GET',--}}
{{--                            'url' => route('project.image.index', ['project' => $project->id]),--}}
{{--                            'class' => 'form-inline my-2 my-lg-0 float-right',--}}
{{--                            'role' => 'search'--}}
{{--                        ])  !!}--}}
{{--                        <div class="input-group">--}}
{{--                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">--}}
{{--                            <span class="input-group-append">--}}
{{--                                <button class="btn btn-secondary" type="submit">--}}
{{--                                    <i class="fa fa-search"></i>--}}
{{--                                </button>--}}
{{--                            </span>--}}
{{--                        </div>--}}
{{--                        {!! Form::close() !!}--}}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width='300px'>Name En</th>
                                        <th>Sort Order</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($projectimages as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name_en }}</td>
                                        <td>{{ $item->sort_order }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            <a href="{{ route('project.image.show', [
                                                'project' => $project->id,
                                                'image' => $item->id
                                            ]) }}"
                                               title="View ProjectImage"
                                            ><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/project/' . $project->id . '/image/' . $item->id .'/edit') }}"
                                               title="Edit ProjectImage">
                                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => route('project.image.destroy', [
                                                    'project' => $project->id,
                                                    'image' => $item->id
                                                ]),
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete ProjectImage',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $projectimages->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
