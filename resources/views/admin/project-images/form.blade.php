    <div class="form-group{{ $errors->has('name_en') ? 'has-error' : ''}}">
    {!! Form::label('name_en', 'Name En', ['class' => 'control-label']) !!}
    {!! Form::text('name_en', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name_jp') ? 'has-error' : ''}}">
    {!! Form::label('name_jp', 'Name Jp', ['class' => 'control-label']) !!}
    {!! Form::text('name_jp', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_jp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
    <small>585 x 460</small>
    {!! Form::file('image', ($formMode == 'create') ? ['class' => 'form-control', 'required' => 'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('status', $statuses, null, ('required' == 'required') ? ['class' => 'form-control', 'required' =>
    'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('sort_order') ? 'has-error' : ''}}">
    {!! Form::label('sort_order', 'Sort Order', ['class' => 'control-label']) !!}
    {!! Form::number('sort_order', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::hidden('project_id', $project->id) !!}
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
