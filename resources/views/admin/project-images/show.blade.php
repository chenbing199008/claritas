@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ProjectImage {{ $projectimage->id }}</div>
                    <div class="card-body">

                        <a href="{{ route('project.image.index', ['project' => $project->id]) }}"
                           title="Back"><button class="btn btn-warning btn-sm"><i class="fa
                        fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ route('project.image.edit', [
                                'project' => $project->id,
                                'image' =>$projectimage->id]
                                )}}"
                               title="Edit ProjectImage"
                            ><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => route('project.image.destroy', [
                                'project' => $project->id,
                                'image' => $projectimage->id
                            ]),
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete ProjectImage',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $projectimage->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name En </th>
                                        <td> {{ $projectimage->name_en }} </td>
                                    </tr>
                                    <tr>
                                        <th> Name Jp </th>
                                        <td> {{ $projectimage->name_jp }} </td>
                                    </tr>
                                    <tr>
                                        <th> Image </th>
                                        <td> <img src='/storage/project-image/{{ $projectimage->image }}' width='500'
                                            /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
