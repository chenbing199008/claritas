@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ $gallery->name_en }} <i class='fa fa-angle-right'></i> Gallery Primary Photos <i class='fa fa-angle-right'></i> View Image</div>
                    <div class="card-body">

                        <a href="{{ route('gallery.primary.index', ['gallery' => $gallery->id]) }}"
                           title="Back"><button class="btn btn-warning btn-sm"><i class="fa
                        fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ route('gallery.primary.edit', [
                                'gallery' => $gallery->id,
                                'project' => $gallery->id,
                                'image' => $galleryprimaryphoto->id,
                                'primary' => $galleryprimaryphoto->id
                                ])}}"
                           title="Edit"
                        ><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => route('gallery.primary.destroy', [
                                'gallery' => $gallery->id,
                                'image' => $galleryprimaryphoto->id,
                                'primary' => $galleryprimaryphoto->id
                            ]),
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete ProjectImage',
                                'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $galleryprimaryphoto->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Title </th>
                                        <td> {{ $galleryprimaryphoto->title }}</td>
                                    </tr>
                                    <tr>
                                        <th> Image Large </th>
                                        <td><img src='/storage/gallery-primary-image/{{ $galleryprimaryphoto->image_large }}'> </td></tr>
                                    </tr>
                                    <tr>
                                        <th> Image Thumb </th>
                                        <td><img src='/storage/gallery-primary-image/{{ $galleryprimaryphoto->image_thumb }}'> </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
