<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Project Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image_large') ? 'has-error' : ''}}">
    {!! Form::label('image_large', 'Image Large', ['class' => 'control-label']) !!}
    <small>... x ...</small>
    {!! Form::file('image_large', ($formMode == 'create') ? ['class' => 'form-control', 'required' => 'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('image_large', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image_thumb') ? 'has-error' : ''}}">
    {!! Form::label('image_thumb', 'Image Thumbnail', ['class' => 'control-label']) !!}
    <small>... x ...</small>
    {!! Form::file('image_thumb', ($formMode == 'create') ? ['class' => 'form-control', 'required' => 'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('image_thumb', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('outside_text') ? 'has-error' : ''}}">
    {!! Form::label('outside_text', 'Outside Text (city, country)', ['class' => 'control-label']) !!}
    <small>The text that displays on the white screen</small>
    {!! Form::textarea('outside_text', null, ['class' => 'form-control']) !!}
    {!! $errors->first('outside_text', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('inside_text') ? 'has-error' : ''}}">
    {!! Form::label('inside_text', 'Inside Text (Providing)', ['class' => 'control-label']) !!}
    <small>Optional: The text that displays on the black screen on pop up</small>
    {!! Form::textarea('inside_text', null, ('' == 'required') ? ['class' => 'form-control'] : ['class' => 'form-control']) !!}
    {!! $errors->first('inside_text', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('sort_order') ? 'has-error' : ''}}">
    {!! Form::label('sort_order', 'Sort Order', ['class' => 'control-label']) !!}
    {!! Form::number('sort_order', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::hidden('gallery_id', $gallery->id) !!}
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
