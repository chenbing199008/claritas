@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ $gallery->name_en }} <i class='fa fa-angle-right'></i> Gallery Primary Photos <i class='fa fa-angle-right'></i> Edit Image</div>
                    <div class="card-body">
                        <a href="{{ route('gallery.primary.index', ['gallery' => $gallery->id]) }}"
                           title="Back"><button class="btn btn-warning btn-sm"><i class="fa
                        fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($galleryprimaryphoto, [
                            'method' => 'PATCH',
                            'url' => route(
                            'gallery.primary.update',
                            [
                                'gallery' => $gallery->id,
                                'image' => $galleryprimaryphoto->id,
                                'primary' => $galleryprimaryphoto->id,
                            ]),
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.gallery-primary-photos.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
