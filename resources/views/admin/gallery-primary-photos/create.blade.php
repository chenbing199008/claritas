@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ $gallery->name_en }} <i class='fa fa-angle-right'></i> Gallery Primary Photos <i class='fa fa-angle-right'></i> Add New Image</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/gallery/' . $gallery->id . '/primary') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open([
                            'url' => route('gallery.primary.store', ['gallery' => $gallery->id]),
                            'class' => 'form-horizontal',
                            'files'=>true,
                            ])
                        !!}

                        @include ('admin.gallery-primary-photos.form', ['formMode' => 'create'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
