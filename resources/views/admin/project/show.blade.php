    @extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Project {{ $project->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/project') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/project/' . $project->id . '/edit') }}" title="Edit Project"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/project', $project->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Project',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $project->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name En </th>
                                        <td> {{ $project->name_en }} </td>
                                    </tr>
                                    <tr>
                                        <th> Name Jp </th>
                                        <td> {{ $project->name_jp }} </td>
                                    </tr>
                                    <tr>
                                        <th> Year </th>
                                        <td> {{ $project->year }} </td>
                                    </tr>
                                    <tr>
                                        <th> Address </th>
                                        <td> {{ $project->address }} </td>
                                    </tr>
                                    <tr>
                                        <th> Country</th>
                                        <td> {{ $project->country->name_en ?? 'PLEASE ADD COUNTRY'}} </td>
                                    </tr>
                                    <tr>
                                        <th> Providing</th>
                                        <td> {{ $project->categories->pluck('name_en') }} </td>
                                    </tr>
                                    <tr>
                                        <th> Event Category</th>
                                        <td> {{ $project->event_category_free_description ?? $project->eventCategories->pluck('name_en') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
