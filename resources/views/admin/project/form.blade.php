<div class="form-group{{ $errors->has('name_en') ? 'has-error' : ''}}">
    {!! Form::label('name_en', 'Name En *', ['class' => 'control-label']) !!}
    {!! Form::text('name_en', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name_jp') ? 'has-error' : ''}}">
    {!! Form::label('name_jp', 'Name Jp *', ['class' => 'control-label']) !!}
    {!! Form::text('name_jp', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_jp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('country_id') ? 'has-error' : ''}}">
    {!! Form::label('country_id', 'Country  *', ['class' => 'control-label']) !!}
    {!! Form::select('country_id', $countries, null, ('required' == 'required') ? ['class' => 'form-control',
    'required' =>
    'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('year') ? 'has-error' : ''}}">
    {!! Form::label('year', 'Year*', ['class' => 'control-label']) !!}
    {!! Form::text('year', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address *', ['class' => 'control-label']) !!}
    {!! Form::text('address', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('categories') ? 'has-error' : ''}}">
    {!! Form::label('categories', 'Category (Providing)', ['class' => 'control-label']) !!}
    {!! Form::select('categories[]', $categories, $project->categories ?? null , [
        'class' => 'form-control',
        'multiple' => 'multiple',
        'style' => 'height: 310px;'
    ]) !!}
    <small>You can select multiple categories by pressing ctrl key</small>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('event_category') ? 'has-error' : ''}}">
    {!! Form::label('event_category', 'Event Category', ['class' => 'control-label']) !!}
    {!! Form::select('event_category[]', $eventCategories, $project->eventCategories ?? null , [
        'class' => 'form-control',
        'multiple' => 'multiple',
        'style' => 'height: 340px;'
    ]) !!}
    <small>You can select multiple categories by pressing ctrl key</small>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('event_category_free_description') ? 'has-error' : ''}}">
    {!! Form::label('event_category_free_description', 'Event Category (Free Description)', ['class' => 'control-label']) !!}
    {!! Form::text('event_category_free_description', null, ['class' => 'form-control']) !!}
    <small>If this free description is filled, it will override Event Category option chosen above</small>
    {!! $errors->first('event_category_free_description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status *', ['class' => 'control-label']) !!}
    {!! Form::select('status', $statuses, null, ('required' == 'required') ? ['class' => 'form-control', 'required' =>
    'required'] :
    ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('sort_order') ? 'has-error' : ''}}">
    {!! Form::label('sort_order', 'Sort Order', ['class' => 'control-label']) !!}
    {!! Form::number('sort_order', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
