
<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('summary') ? 'has-error' : ''}}">
    {!! Form::label('summary', 'Summary', ['class' => 'control-label']) !!}
    {!! Form::text('summary', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('summary', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group"><label for="summary" class="control-label">Body</label>
    <textarea name="body"  style="width:100%;height:600px;" class="crud-richtext">
        @if(isset($news))
            {{$news->body}}
        @endif
    </textarea>
</div>

{{--<div class="form-group{{ $errors->has('body') ? 'has-error' : ''}}">--}}
    {{--{!! Form::label('body', 'Body', ['class' => 'control-label']) !!}--}}
    {{--{!! Form::textarea('body', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}--}}
    {{--{!! $errors->first('body', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}


<div class="form-group{{ $errors->has('hash_tags') ? 'has-error' : ''}}">
    {!! Form::label('hash_tags', 'Hash Tags (Multiple tags have been separated by commas)', ['class' => 'control-label']) !!}
    {!! Form::text('hash_tags', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('hash_tags', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('link') ? 'has-error' : ''}}">
    {!! Form::label('link', 'Link', ['class' => 'control-label']) !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('link_name') ? 'has-error' : ''}}">
    {!! Form::label('link_name', 'Link Name', ['class' => 'control-label']) !!}
    {!! Form::text('link_name', null, ['class' => 'form-control']) !!}
    {!! $errors->first('link_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('published_at') ? 'has-error' : ''}}">
    {!! Form::label('published_at', 'Published On', ['class' => 'control-label']) !!}
    {!! Form::input('date', 'published_at', $now ?? date('Y-m-d', strtotime($news->published_at)), ['class' => 'form-control','required' => 'required']) !!}
    {!! $errors->first('posted_date', '<p class="help-block">:message</p>') !!}
</div>




<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
