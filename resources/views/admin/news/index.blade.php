@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">News</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/news/create') }}" class="btn btn-success btn-sm" title="Add News">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add News
                        </a>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Body</th>
                                        {{--<th>Hash Tags</th>--}}
                                        {{--<th>Link</th>--}}
                                        {{--<th>Link Name</th>--}}
                                        <th>Published On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($news as $index => $item)
                                    <tr>
                                        <td>{{$loop->iteration }}</td>
                                        <td title="{{$item->title}}">{{ $item->cutTitle }}</td>
                                        <td title="{{$item->body}}">{{ $item->cutBody }}</td>
                                        {{--<td>--}}
                                            {{--@foreach($item->hashTags as $tag)--}}
                                                {{--<div class="label label-default">{{$tag}}</div>--}}
                                            {{--@endforeach--}}
                                        {{--</td>--}}
                                        {{--<td>{{ $item->link }}</td>--}}
                                        {{--<td>{{ $item->link_name }}</td>--}}
                                        <td>{{ date('m/d/Y', strtotime($item->published_at)) }}</td>
                                        <td style="width: 140px;">
                                            <a href="{{ url('/admin/news/' . $item->id) }}" title="View News"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/news/' . $item->id . '/edit') }}" title="Edit News"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/news', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-sm',
                                                    'title' => 'Delete News',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="pagination-wrapper"> {!! $news->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
