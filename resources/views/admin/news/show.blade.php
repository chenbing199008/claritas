@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">News {{ $news->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/news') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/news/' . $news->id . '/edit') }}" title="Edit Blog"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $news->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Published On </th>
                                        <td>{{ date('m/d/Y', strtotime($news->published_at)) }}</td>
                                    </tr>

                                    <tr>
                                        <th> Title </th><td> {{ $news->title }} </td>
                                    </tr>
                                    <tr>
                                        <th> Summary </th><td> {{ $news->summary }} </td>
                                    </tr>
                                    <tr>
                                        <th> Body </th><td> {{ $news->body }} </td>
                                    </tr>


                                    <tr>
                                        <th> Hash Tags </th><td>
                                            @foreach($news->hashTags as $tag)
                                                <div class="label label-default">{{$tag}}</div>
                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <th> Link </th><td> {{ $news->link }} </td>
                                    </tr>

                                    <tr>
                                        <th> Link Name </th><td> {{ $news->link_name }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
