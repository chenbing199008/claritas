
new Vue({
    el: '#contact-info',
    data() {
        return {
            language : window.language,
            sending: false,
            form: {
                office: 2,
                name: '',
                companyName: '',
                tel: '',
                email: '',
                message: '',
                protection: false,
            },
            errorMessages: [],

            errors: {
                name: false,
                companyName: false,
                tel: false,
                email: false,
                message: false,
            },

            anyError: false,
            hasReadAll: false,
            showUnReadNotice: false,
            unReadAllText: false,
        }
    },

    methods: {
        send() {
            if (this.sending) {
                return;
            }

            if (!this.form.protection) {
               this.showUnReadNotice = true;
            }

            this.anyError = false;
            this.sending = true;
            this.errorMessages = [];

            this.errors = {
                name: false,
                companyName: false,
                tel: false,
                email: false,
                message: false,
            };

            const ERROR_NAME = 1;
            const ERROR_COMPANY_NAME = 2;
            const ERROR_TEL = 3;
            const ERROR_EMAIL = 4;
            const ERROR_MESSAGE = 5;

            if (!this.form.name.trim()) {
                this.errorMessages.push(ERROR_NAME);
                this.errors.name = true;
                this.anyError = true;
                this.sending = false;
            }

            if (!this.form.companyName.trim()) {
                this.errorMessages.push(ERROR_COMPANY_NAME);
                this.errors.companyName = true;
                this.anyError = true;
                this.sending = false;
            }

            if (!this.form.tel.trim()) {
                this.errorMessages.push(ERROR_TEL);
                this.errors.tel = true;
                this.anyError = true;
                this.sending = false;
            }

            if (!this.form.email.trim() || !this.validEmail(this.form.email.trim())) {
                this.errorMessages.push(ERROR_EMAIL);
                this.errors.email = true;
                this.anyError = true;
                this.sending = false;
            }

            if (!this.form.message.trim()) {
                this.errorMessages.push(ERROR_MESSAGE);
                this.errors.message = true;
                this.anyError = true;
                this.sending = false;
            }

            if (!this.form.protection && this.isJp) {
                this.anyError = true;
            }

            if (this.anyError) {
                return;
            }

            this.sending = false;

            var url = '/contact_sent';
            if (this.language === 'ja') {
                var url = '/ja/contact_sent';
            }

            return axios.post(url, this.form)
                .then(response => {
                    window.location.href = response.data.url;
                });
        },

        validEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        handleScroll(e) {
            const { scrollTop, offsetHeight, scrollHeight } = e.target;
            if ((scrollTop + offsetHeight) >= scrollHeight) {
                this.hasReadAll = true;
                this.unReadAllText = false;
            }
        },

        showUnReadAllNotice() {
            if (!this.hasReadAll)  {
                this.unReadAllText = true;
            } else {
                this.showUnReadNotice = false;
            }
        },

        resetForm() {
            if (this.language === 'ja') {
                this.form.office = 1;
            }
        }
    },

    created: function () {
        this.resetForm();
    },
})

Vue.directive('scroll', {
    inserted: function(el, binding) {
        let f = function(evt) {
            if (binding.value(evt, el)) {
                window.removeEventListener('scroll', f);
            }
        }
        window.addEventListener('scroll', f);
    }
});
