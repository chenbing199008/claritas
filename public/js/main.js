(function($) {
  // Custom scrollbars
  $('#photo-blog-detail').overlayScrollbars({
    scrollbars: {
      visibility: "hidden"
    }
  });

  $('.country-select-form .project-listings').overlayScrollbars({
    className: "os-theme-dark"
  });

  // Hamburger
  $('.navbar-toggler').click(function() {
    $('.bt-menu-trigger').toggleClass('bt-menu-open');
  });

  // Home banner
  $('.home-banner-init , .gallery-info-big-slider-init').slick({
    adaptiveHeight: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    fade: true,
    infinite: true,
    mobileFirst: true,
    navs: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToshow: 1,
    slidesToScroll: 1,
    speed: 800,
    swipe : false
  });

  navHeight = $(".main-navigation").height();
  // Image Popup
  $(".gallery-thumb-slider, .gallery-info-big-slider-wrap").each(function() {
    $(this).find('a').jqPhotoSwipe({
      forceSingleGallery: true,
      loop: true,
      history: false,
      closeOnVerticalDrag: true,
      closeOnScroll: false,
      zoomEl: false,
      fullscreenEl: false,
      shareEl: false,
      tapToClose: false,
      counterEl: false,
      getDoubleTapZoom: function(isMouseClick, item) {
        return item.initialZoomLevel;
      },
      bgOpacity: 0.9,
      timeToIdle: 7000,
      barsSize: {top: 44, bottom: 200},
      galleryOpen: function (gallery) {
        navHeight = $(".main-navigation").height();
        $(".pswp").css("top", navHeight);
        $(".pswp__caption").css("bottom", navHeight);
      }
    });
  });

  // Scroll To
  $('a[href="#scroll-to-section"]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top + 1}, 500, 'linear');
  });

  // Homebanner
  function homeBannerEffect() {
    var homeBannerItemHeight = $('.home-banner-item').outerHeight();
    var mainNavigationHeight = $('.main-navigation').outerHeight();
    $('.home-banner').css('height', homeBannerItemHeight - mainNavigationHeight);
  }
  $(document).ready(homeBannerEffect);
  $(window).resize(homeBannerEffect);

  //navigation stick
  function scrollStickEffect() {
    $(window).scroll(function () {
      var headHeight = $('.home-banner').outerHeight();
      var scrollTop = $(window).scrollTop();
      if (scrollTop > headHeight) {
          $('.main-navigation').addClass('sticky-top');
      } else {
          $('.main-navigation').removeClass('sticky-top');
      }
    });
  }

  $(document).ready(scrollStickEffect);
  $(window).resize(scrollStickEffect);

  // Photo blog
  $('.photo-blog-item').on("click", function() {

    $("#blog-detail-title").text($(this).data("title"))
    $("#blog-detail-date").text($(this).data("date"))
    $("#blog-detail-img").attr('src', $(this).data("image"))
    $("#blog-detail-post").html($(this).data("post"))

    $('#photo-blog-detail').addClass('photo-blog-detail-active');
    $('.photo-blog-item').addClass('photo-blog-opacity');
    $('.photo-blog-item').removeClass('photo-blog-opaque');
    if ( $(this).closest('.photo-blog-item').hasClass('photo-blog-opacity') ) {
      $(this).closest('.photo-blog-item').removeClass('photo-blog-opacity');
      $(this).closest('.photo-blog-item').addClass('photo-blog-opaque');
    } else if ( $(this).closest('.photo-blog-item').hasClass('photo-blog-opaque') ) {
      $(this).closest('.photo-blog-item').removeClass('photo-blog-opaque');
      $(this).closest('.photo-blog-item').addClass('photo-blog-opacity');
    }
  });

  $('.photo-blog-detail-close').click(function() {
    $('#photo-blog-detail').removeClass('photo-blog-detail-active');
    $('.photo-blog-item').removeClass('photo-blog-opacity');
    $('.photo-blog-item').removeClass('photo-blog-opaque');
  });

})(jQuery);
