new Vue({
    el: '#main-wrapper',
    data() {
        return {
            loading: false,
            posts: [],
            latestPost: {},
            latestPostBody: '',
            showLatestPostBody: false,
            total: 0,
            timeLines: [],

            showLatest: true,

            query: {
                keyWord: '',
                page: 1,
                date: '',
            },
            previewId: '',
        }
    },

    methods: {
        getPostsByKeyWord() {
            this.posts = [];
            this.query.date = '';
            this.query.page = 1;
            this.showLatest = false;
            this.getPosts();
        },

        searchByMonth(date) {
            this.posts = [];
            this.query.date = date;
            this.query.page = 1;
            this.showLatest = false;
            this.getPosts();
        },
        readMore(post) {
            post.mockBody = post.originBody;
            post.showAll = true;
        },

        lessMore(post) {
            post.mockBody = post.body;
            post.showAll = false;
        },

        lessLatestMode() {
            this.latestPostBody = this.latestPost.body;
            this.showLatestPostBody = false;
        },

        readLatestMode() {
            this.latestPostBody = this.latestPost.originBody;
            this.showLatestPostBody = true;
        },

        pageChange(page) {
            this.query.page = page;
            this.getPosts();
        },

        preview(id) {
            this.previewId = id;
            this.dialogPreview = true;
        },


        getLatestPost() {
            this.showLatest = true;
            return axios.get("/news/latest")
                .then(response => {
                    this.latestPost = response.data.latest;
                });
        },

        getPosts() {
            this.loading = true;
            return axios.post("/news/posts", {query: this.query})
                .then(response => {
                    this.posts = response.data.posts;
                    this.total = response.data.total;
                    this.loading = false;
                });
        },

        getTimeLines() {
            return axios.get("/news/timeLines")
                .then(response => {
                    this.timeLines = response.data.timeLines;
                });
        },

    },

    created: function () {
        this.getLatestPost();
        this.getPosts();
        this.getTimeLines();
    },
})