new Vue({
    el: '#main-wrapper',
    data() {
        return {
            selectedDate: '',
            loading: false,
            posts: [],
            latestPost: {},
            latestPostBody: '',
            showLatestPostBody: false,
            total: 0,
            timeLines: [],

            query: {
                keyWord: '',
                page: 1,
                date: '',
            },
            previewId: '',
            showLatest: true,
        }
    },

    methods: {

        changeDate() {
            this.query.page = 1;
            this.query.date = this.selectedDate;
            this.showLatest = false;
            this.getPosts();
        },

        getPostsByKeyWord() {
            this.posts = [];
            this.query.page = 1;
            this.showLatest = false;
            this.getPosts();
        },

        readMore(post) {
            post.mockBody = post.originBody;
            post.showAll = true;
        },

        lessMore(post) {
            post.mockBody = post.body;
            post.showAll = false;
        },

        lessLatestMode() {
            this.latestPostBody = this.latestPost.body;
            this.showLatestPostBody = false;
        },

        readLatestMode() {
            this.latestPostBody = this.latestPost.originBody;
            this.showLatestPostBody = true;
        },

        pageChange(page) {
            this.query.page = page;
            this.getPosts();
        },

        preview(id) {
            this.previewId = id;
            this.dialogPreview = true;

        },


        getLatestPost() {
            axios.get("/news/latest")
                .then(response => {
                    this.latestPost = response.data.latest;
                    this.latestPostBody = this.latestPost.body;
                });
        },

        getPosts() {
            this.loading = true;
            axios.post("/news/posts", {query: this.query})
                .then(response => {
                    this.posts = response.data.posts;
                    this.total = response.data.total;
                    this.loading = false;
                });
        },

        getTimeLines() {
            axios.get("/news/mobileTimeLines")
                .then(response => {
                    this.timeLines = response.data.timeLines;
                });
        },

    },

    created: function () {
        this.getLatestPost();
        this.getPosts();
        this.getTimeLines();
    },
})
