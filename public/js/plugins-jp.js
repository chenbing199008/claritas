// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
// -- Map.js
( function() {
  window.WorldMap = function( $worldMap ) {

      this.areaCoords = window.APP.areaCoords;
      this.areas = window.APP.areas;
      this.areaImageBaseUrl = window.APP.areaImageBaseUrl;
      this.activeArea = null;

      this.$worldMap = $worldMap;
      this.$map = $("map", $worldMap);

      this.plotAreas();
      this.addAreaImages();
      this.$map.imageMapResize();
      this.$worldMap.data( 'world-map', this );

  };

  WorldMap.prototype = {

      createArea: function( area ) {
          var $area = $( '<area href="#" coords="' + this.areaCoords[area.name] + '" shape="poly">' );
          $area
              .click( ( function( e ) {

                  e.preventDefault();
                  this.setActiveArea( area );

              } ).bind( this ) )
              .hover( ( function() {

                  if( !this.activeArea || ( area.name !== this.activeArea.name ) ) {
                      this.showAreaImage( area );
                  }

              } ).bind( this ), ( function() {

                  if( !this.activeArea || ( area.name !== this.activeArea.name ) ) {
                      this.hideAreaImage( area );
                  }

              } ).bind( this ) );
          return $area;
      },

      createAreaImage: function( area ) {
          var src = this.areaImageBaseUrl + area.name + '.svg';
          return $( '<div class="area-image area-image-hidden area-image-' + area.name + '"><img src="' + src  + '"></div>' );
      },

      addAreaImages: function() {

          for( var i in this.areas ) {
              this.$worldMap.append( this.createAreaImage( this.areas[i] ) );
          }

      },

      plotAreas: function() {
          for( var i in this.areas ) {
              this.$map.append( this.createArea( this.areas[i] ) );
          }
      },

      getAreaElement: function( area ) {

          return $( '.area-image-' + area.name, this.$worldMap );

      },

      showAreaImage: function( area ) {

          this.getAreaElement( area ).removeClass( 'area-image-hidden' );

      },

      hideAreaImage: function( area ) {

          this.getAreaElement( area ).addClass( 'area-image-hidden' );

      },

      setActiveArea: function( area ) {

          if( area === undefined ) {
              area = null;
          }

          if( ( area === this.activeArea ) || ( area && this.activeArea && area.name === this.activeArea.name ) ) {
              return;
          }

          if( this.activeArea ) {
              this.hideAreaImage( this.activeArea );
          }
          this.activeArea = area;
          if( this.activeArea ) {
              this.showAreaImage( area );
          }
          this.$worldMap.trigger( 'change', area );

      }
  };
} )();
// -- //Map.js

// -- Project.js
// Area Select
( function() {

  window.AreaSelect = function( $select ) {

      this.areas = window.APP.areas;
      this.$select = $select;

      this.insertOptions();
      this.$select.data( 'area-select', this );

  };

  AreaSelect.prototype = {

      createOption: function( area ) {

          return $( '<option value="' + area.name + '" >' + area.label + '</option>' );

      },

      insertOptions: function(){

          for( var i in this.areas ) {

              var $option = this.createOption( this.areas[i] );
              this.$select.append( $option );

          }

      },

      isAreaSelected: function( area ) {

          var value = $( 'option:selected', this.$select ).val();
          if( value === '' ) {
              value = null;
          }
          return ( value === area ) || ( value === area.name );

      },

      select: function( area ) {

          if( area === undefined ) {
              area = null;
          }
          if( !this.isAreaSelected( area ) ) {
              this.$select.val( area.name );
              this.$select.trigger( 'change' );
          }

      },

      getSelectedArea: function() {

          var areaName = this.$select.val();
          var area = this.areas.find( function( area ) {
              return areaName === area.name;
          } );
          return area ? area : null;

      }

  };

} )();

// Country Select
( function() {

  window.CountrySelect = function( $select ) {

      this.countries = window.APP.countries;
      this.areas = window.APP.areas;
      this.area = null;
      this.$select = $select;

      this.insertCountryOptions();
      this.$select.data( 'country-select', this );

  };

  CountrySelect.prototype = {

      getSelectedArea: function() {
          return this.area;
      },

      getSelectedCountry: function() {

          var value = this.$select.val();
          if( value === '' || value === undefined ) {
              return null;
          }
          return this.getCountryByName( value );

      },

      refreshSelectedArea: function() {

          var value = this.$select.val();

          // If value is not selected the no need to change the area;
          if( value === '' || !value ) {
              return;
          }

          // Get the selected country object
          var country = this.getCountryByName( value );

          // Find area and set area
          var area;
          if( country ) {
              area = this.areas.find( ( function( a ) {
                  return a.name === country.area;
              } ).bind( this ) );
          }
          area = area ? area : null;
          this.setArea( area );
      },

      setArea: function( area ) {

          if( area === undefined ) {
              area = null;
          }

          if( ( area === this.area ) || ( area && this.area && this.area.name === area.name ) ) {
              return;
          }

          this.area = area;
          this.refreshOptions();

      },

      getCountryByName: function( name ) {

          return this.countries.find( ( function( country ) {
              return country.name === name;
          } ).bind( this ) );

      },

      getCountries: function() {

          var countries = [];
          if( this.area ) {
              countries = this.countries.filter( ( function( country ) {
                  return country.area === this.area.name;
              } ).bind( this ) );
          }else {
              countries = this.countries;
          }
          return countries;

      },

      removeOptions: function() {

          var countries = this.getCountries();
          $( 'option:not(:nth-child(1))', this.$select ).remove();

      },

      refreshOptions: function() {

          this.removeOptions();
          this.insertCountryOptions();

      },

      createCountryOption: function( country ) {

          return $( '<option value="' + country.name + '">' + country.label + '</option>' );

      },

      insertCountryOptions: function() {

          var countries = this.getCountries();
          for( var i in countries ) {
              var $option = this.createCountryOption( countries[i] );
              this.$select.append( $option );
          }

      }

  }

} )();

// -- Map.js
( function() {

  var $worldMap = $( '#world-map' );
  var worldMap = new WorldMap( $worldMap );

  var $areaSelect = $( "#area-select" );
  var areaSelect = new AreaSelect( $areaSelect );

  var $countrySelect = $( "#country-select" );
  var countrySelect = new CountrySelect( $countrySelect );

  function getCountriesByArea( area ) {
      if( area ){
          return window.APP.countries.filter( function( c ) {
              return c.area === area.name;
          } );
      }else {
          return [];
      }
  }

// Change events
  $worldMap.on( 'change', function( e, area ) {
      areaSelect.select( area );
  } );

  $areaSelect.on( 'change', function( e ) {
      var area = areaSelect.getSelectedArea();
      worldMap.setActiveArea( area );
      countrySelect.setArea( area );
      filterProjectTableByArea( area );
  });
} )();
// -- //Map.js