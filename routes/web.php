<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



$optionalRoutes = function () {
    Route::get('/', 'FrontendController@index');
    Route::get('/data.js', 'FrontendController@data');
    Route::get('/work', 'FrontendController@work');
    Route::get('/contact', 'FrontendController@contact');
    Route::get('/gallery', 'FrontendController@gallery');
    Route::get('/photo_blog', 'FrontendController@photoblog');
    Route::get('/industry_news', 'NewsController@index');
    Route::get('/privacy{extension}', 'PolicyController@index')->where('extension', '(?:.html)?');
    Route::post('contact_sent', 'FrontendController@contactSent');
    Route::post('/ja/contact_sent', 'FrontendController@contactSent');
    Route::get('/contact_sent_success', 'FrontendController@contactSentSuccess');
    Route::get('/ja/contact_sent_success', 'FrontendController@contactSentSuccess');
};

Route::get('/news/latest', 'NewsController@latest');
Route::post('/news/posts', 'NewsController@posts');
Route::get('/news/timeLines', 'NewsController@timeLines');
Route::get('/news/mobileTimeLines', 'NewsController@mobileTimeLines');




Route::group(['middleware' => ['preferred.language'], 'prefix' => '{lang?}', 'where' => ['lang' => 'en|ja']], $optionalRoutes);

Route::group(['middleware' => ['preferred.language']], $optionalRoutes);

Auth::routes();

// Check role in route middleware
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => 'admin'], function () {
    Route::get('', 'AdminController@index');
//    Route::resource('roles', 'RolesController');
//    Route::resource('permissions', 'PermissionsController');
    Route::resource('users', 'UsersController');
    Route::resource('activitylogs', 'ActivityLogsController')->only([
        'index', 'show', 'destroy'
    ]);
    Route::resource('settings', 'SettingsController');
    Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

    Route::resource('area', 'AreaController');
    Route::resource('news', 'NewsController');
    Route::resource('country', 'CountryController');
    Route::resource('category', 'CategoryController');
    Route::resource('project', 'ProjectController');
    Route::resource('blog', 'BlogController');
    Route::resource('project.image', 'ProjectImagesController');
    Route::resource('gallery', 'GalleryController');
    Route::resource('gallery.primary', 'GalleryPrimaryPhotosController');
//    Route::resource('gallery.secondary', 'GallerySecondaryPhotosController');
});
Route::resource('admin/event-categories', 'Admin\\EventCategoriesController');
Route::resource('admin/project-event-categories', 'Admin\\ProjectEventCategoriesController');

//Route::resource('admin/gallery-primary-photos', 'Admin\\GalleryPrimaryPhotosController');
//Route::resource('admin/gallery-secondary-photos', 'Admin\\GallerySecondaryPhotosController');
