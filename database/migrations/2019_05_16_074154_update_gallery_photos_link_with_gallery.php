<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGalleryPhotosLinkWithGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_primary_photos', function (Blueprint $table) {
            $table->integer('gallery_id')->unsigned();
            $table
                ->foreign('gallery_id')
                ->references('id')
                ->on('galleries')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });

        Schema::table('gallery_secondary_photos', function (Blueprint $table) {
            $table->integer('gallery_id')->unsigned();
            $table
                ->foreign('gallery_id')
                ->references('id')
                ->on('galleries')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
