<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name_en')->nullable();
            $table->string('name_jp')->nullable();
            $table->integer('country_id')->unsigned();
            $table->string('year')->nullable();
            $table->string('address')->nullable();
            $table->string('status')->nullable();
            $table->integer('sort_order')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
