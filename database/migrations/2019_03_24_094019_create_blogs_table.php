<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title_en')->nullable();
            $table->string('title_jp')->nullable();
            $table->string('image_thumb')->nullable();
            $table->string('image_large')->nullable();
            $table->text('excerpt_en')->nullable();
            $table->text('excerpt_jp')->nullable();
            $table->text('post_en')->nullable();
            $table->text('post_jp')->nullable();
            $table->dateTime('posted_date')->nullable();
            $table->string('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');
    }
}
