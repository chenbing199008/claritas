<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGalleryImageField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_primary_photos', function (Blueprint $table) {
            $table->text('outside_text');
            $table->text('inside_text')->nullable();;
        });

        Schema::table('gallery_secondary_photos', function (Blueprint $table) {
            $table->text('outside_text');
            $table->text('inside_text')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
