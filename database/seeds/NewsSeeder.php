<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ja_JP');

        foreach (range(1,40) as $index) {
            $tags = [];

            for ($i = 0; $i < 4; $i++) {
                $tags[] = $faker->word;
            }

            DB::table('news')->insert([
                'title' => $faker->sentence,
                'body' => $faker->paragraph(10),
                'summary' => $faker->sentence,
                'hash_tags' => implode(',', $tags),
                'link_name' => $faker->name,
                'link' => $faker->url,
                'published_at' => $faker->date(),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }
    }
}
